#ifndef PARAMETER
#define PARAMETER
#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<fstream>
#include<string>
#include<vector>
using namespace std;
class parameter
{
public:
	//memory initiale parameters
	static int wl_length;
	static int bl_length;
	static double sram_height;
	static double sram_width;
	static double cnt_length;
	static double cnt_length_deviation;
	static double pitch;
	static double pitch_deviation;
	static double sram_number;
	//sram layout parameters
	
	//transistor layout parameters
	static double trans_height;
	static double trans_width;
	static double tran_height_pitch;
	static double tran_width_pitch;
	static int test_step;
	static int initialization_test_step;
	static double word_width;
	static long metallic_ratio;
	static long slope_ratio;
    static int level_num;
    static double redundant_ratio;
    static int max_redundant_r;
    static int max_redundant_c;
    static void readParameters(char *filename);
	//stuck-open switch. 0 close, 1 open.
	static int stuck_open_switch;

};
#define TRANS_PER_SRAM 6
double Random_U();
void dump_parameter();
void clear_sample_dump(vector<int> test_step_v,vector<int> jump_threshold_v,vector<long> metallic_v);



#endif
