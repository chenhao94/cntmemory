#ifndef SETPARAMETER
#define SETPARAMETER
#include<fstream>
#include<iostream>
#include<string>
#include<stdlib.h>
#include<vector>
using namespace std;
void fetch()
{
	ifstream input;
	input.open("input_case.txt");

	vector<int> metallic_v;
	vector<int> test_step_v;
	vector<int> jump_threshold_v;
	vector<int> length_v;

	string metallic_flag = "metallic";
	string test_flag = "test_step";
	string just_flag = "jump_threshold";
	string length_flag = "length";

	string input_string = "ltj";
	const char *p;
	int input_int;
	while(!input.eof())
	{
		input>>input_string;
		p = input_string.c_str();
		if(input_string == metallic_flag)
		{
			input>>input_string;
			while((input_string != just_flag)&&(input_string != test_flag)&&(input_string != length_flag))
			{
				input_int = atoi(input_string.c_str());
				metallic_v.push_back(input_int);
				input>>input_string;
			}
		}
		if(input_string == test_flag)
		{
			input>>input_string;
			while((input_string != just_flag)&&(input_string != metallic_flag)&&(input_string != length_flag))
			{
				input_int = atoi(input_string.c_str());
				test_step_v.push_back(input_int);
				input>>input_string;
			}
		}
		if(input_string == just_flag)
		{
			input>>input_string;
			while((input_string != test_flag)&&(input_string != metallic_flag)&&(input_string != length_flag))
			{
				input_int = atoi(input_string.c_str());
				test_step_v.push_back(input_int);
				input>>input_string;
			}
		}
		input>>input_string;
		input_int = atoi(input_string.c_str());
		length_v.push_back(input_int);
		cout<<p<<endl;
	}


	for(int i = 0; i < metallic_v.size();i++)
	{
		cout<<metallic_v[i]<<endl;
	}

}

void fetchData(vector<long> &metallic_v, vector<int> &test_step_v, vector<int> &jump_threshold_v,vector<double> &length_v)
{
	ifstream input;
	input.open("./input_case/input_case.txt");


/*	vector<int> metallic_v;
	vector<int> test_step_v;
	vector<int> jump_threshold_v;
	vector<double> length_v;
*/
	string metallic_flag = "metallic";
	string test_flag = "test_step";
	string jump_flag = "jump_threshold";
	string length_flag = "length";

	string input_string;
	const char *p;
	int input_int;
	double input_float;
	long input_long;
	int tag_flag = 0;
	while(!input.eof())
	{
		input>>input_string;
		if(input_string == metallic_flag)
		{
			tag_flag = 1;
			continue;
		}
		else if(input_string == test_flag)
		{
			tag_flag = 2;
			continue;
		}
		else if(input_string == jump_flag)
		{
			tag_flag = 3;
			continue;
		}
		else if(input_string == length_flag)
		{
			tag_flag = 4;
			continue;
		}
//		cout<<input_string<<endl;
		if(tag_flag == 1)
		{
			input_long = atol(input_string.c_str());
			//cout<<"++++++input_long = " <<input_long<<endl;
			
		}
		else if(tag_flag == 4)
		{
			input_float = atof(input_string.c_str());
		}else
		{
			input_int = atoi(input_string.c_str());
		}

		switch(tag_flag)
		{
		case 1: 
			metallic_v.push_back(input_long);
			break;
		case 2:
			test_step_v.push_back(input_int);
			break;
		case 3:
			jump_threshold_v.push_back(input_int);
			break;
		case 4:
			length_v.push_back(input_float);
			break;
		default:
			cout<<"No match"<<endl;
		}
	}
	//dump_input_case

	//if(metallic_v.empty()!=true)
	//{
	//	int num=0;
	//	for(vector<int>::size_type it=0;it<metallic_v.size();it++)
	//	{
	//		cout<<"test_step_v.size = "<< test_step_v.size()<<endl;
	//		cout<<"metallic_ratio = " <<metallic_v[it]<<endl;
	//	}
	//}

	//if(test_step_v.empty()!=true)
	//{
	//	for(vector<int>::size_type it=0;it<test_step_v.size();it++)
	//	{
	//		cout<<"test_step = " <<test_step_v[it]<<endl;
	//	}
	//}
	//cout<<"test_step_v size = "<<test_step_v.size()<<endl<<endl;
	//if(jump_threshold_v.empty()!=true)
	//{
	//	for(vector<int>::size_type it=0;it<test_step_v.size();it++)
	//	{
	//		cout<<"jump_threshold_v = " <<jump_threshold_v[it]<<endl;
	//	}
	//}
	//cout << "jump_threshold =" << jump_threshold_v.empty()<<endl;
}
#endif