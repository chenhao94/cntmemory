#ifndef __CNTIMAGE_H__
#define __CNTIMAGE_H__
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <png.h>

typedef unsigned char uint8_t;

typedef struct Pixel
{
	uint8_t r,g,b;

	Pixel(uint8_t _r, uint8_t _g, uint8_t _b) : r(_r), g(_g), b(_b) {}
} Pixel;

typedef class CNTImage
{
	int row, column, width;// all describe the cells, instead of pixels
	Pixel *image;

	public:
	CNTImage(int _r, int _c, int _w);

	~CNTImage() {delete image;}

	void setCell(int _r, int _c, Pixel* pixel);

	void setLine(int _r, Pixel* pixel);

	void printImage(char *filename);

	private:
	void setPixel(int x, int y, Pixel* pixel);
	Pixel *getPixel(int x, int y);
}CNTImage;

#endif
