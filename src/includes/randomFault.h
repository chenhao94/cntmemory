#ifndef RANDOMFAULT
#define RANDOMFAULT
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;
//产生泊松分布（由均匀分布产生泊松分布）
void generatePoissonFault(vector<int>& r,double lambda){
	for(vector<int>::iterator itr=r.begin();itr!=r.end();itr++){
		double t = 0.0;
		int x = 0;
		while(true){
			//srand ( time(NULL) );
			double randoms = rand()/((double)RAND_MAX);
			t = t - log(randoms)/lambda;
			if(t>1.0)
				break;    
			else
				x = x+1;            
		}
		*itr = x;
	}
}
int generateOnePosissonFault(double lambda)
{
	double t = 0.0;
	int x = 0;
	while(true)
	{
		double randoms = rand()/((double)RAND_MAX);
		t = t - log(randoms)/lambda;
		if(t>1.0)
			break;    
		else
			x = x+1;
	}
	return x;

}
bool checkPair(vector<pair<int,int> > &v, int x, int y)
{
	for(vector<pair<int,int> >::iterator itr = v.begin(); itr != v.end(); itr++)
	{
		if((itr->first == x) && (itr->second == y))
			return true;
	}
	return false;
}
void getSattlePosition(vector<pair<int,int> > &v, double sophere, double alpha)
{
	int std_x = (int)sophere + 2;
	int std_y = (int)sophere + 2;
	int i = 0;
	int j = 0;
	int colNumber = 10;
	int rowNumber = 10;

	//此处产生错误
	vector<pair<int,int> >::iterator temp= v.end() - 1;


	int this_fisrt = (temp)->first;
	int this_second = temp->second;


	int left_x = temp->first - std_x;
	int right_x = temp->first + std_x;
	int left_y = temp->second - std_y;
	int right_y = temp->second + std_y;
	if(left_x <= 0)
		left_x = 0;
	if(right_x >= colNumber - 1)
		right_x = colNumber - 1;
	if(left_y <= 0)
		left_y = 0;
	if(right_y >= rowNumber - 1)
		right_y = rowNumber - 1;

	for(i = left_x; i <= right_x; i++)
	{
		for(j = left_y; j <= right_y; j++)
		{
			double dis = abs(i - this_fisrt) + abs(j - this_second);
			if(pow(dis,alpha) < sophere)
			{
				if(!checkPair(v,i,j))
					v.push_back(make_pair(i,j));
			}
		}
	}
}

//lamda is mean of faulty cell,sophere is radius of affected cells , alpha is 0.5
void getFaultPosition(vector<pair<int,int> > &v,double lamda, int rowNumber,int colNumber,double sophere, double alpha )
{
	int insertFault = 0;
	int FaultNum = generateOnePosissonFault(lamda);
	while(insertFault <= FaultNum)
	{
		int x = rand()%rowNumber;
		int y = rand()%colNumber;
		if(!checkPair(v,x,y))
		{
			v.push_back(make_pair(x,y));
			getSattlePosition(v,sophere,alpha);
		}
		insertFault++;
	}
}



#endif
