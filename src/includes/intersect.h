#ifndef INTERSECT
#define INTERSECT

#include <iostream>
using namespace std;


bool is_intersect_t(double line_st_x,double line_st_y,double line_end_x,double line_end_y,double left_bottom_x,double left_bottom_y,double right_top_x,double right_top_y);

#endif