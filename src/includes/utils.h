#ifndef __UTILS_H__
#define __UTILS_H__
#include "bipartite.h"
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define sqr(x) ((x)*(x))
#define cub(x) ((x)*(x)*(x))
#define eps (1e-6)
#define SAMPLE_SIZE 1000
#define REC_PER_SAMPLE 5

class BipGraph;
void gen_faulty_graph(BipGraph &graph);

#endif
