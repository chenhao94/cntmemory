#ifndef MEMORY_H
#define MEMORY_H
#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include<fstream>
#include <iomanip>
#include<math.h>
#include<stdlib.h>
#include<cmath>
#include<queue>
#include<stack>
#include"SetParameter.h"
#include"parameter.h"
#include"randomFault.h"
//#include"line_rectangle_cross.h"
#include"intersect.h"
#include "cntImage.h"
#include "bipartite.h"
#include "utils.h"
using namespace std;

//#define TRANS_PER_SRAM 6

enum attri_t{
	SEMI,
	METAL,
};
bool tag_fault_coveragge;
//sram cell fault
enum fault_t{
	STUCK_AT_0,
	STUCK_AT_1,
	DEFECT_LENGTH,
	DEFECT_SLOPE
};
//trans defect
enum trans_defect_t{
	TRANS_METAL,
	TRANS_LENGTH,
	TRANS_SLOPE
};

//test element
enum test_element{
	R0,
	R1,
	W0,
	W1
};

struct Myresult
{
	int row;
	int column;
	int operation;
};
struct sram_status_t
{
	int store;
	int readout;
};
struct add_pair_t
{
	int low;
	int high;
};

sram_status_t **sram_status;
int delay_hash[13] = {0,3,3,3,3,
						2,2,2,2,
						1,1,1,1};

attri_t is_metal()
{
	
	long k=rand()%parameter::metallic_ratio+1;
	//cout << "k = " << k <<endl;
	if(k <= 1)
	{
		return METAL;
	}
	else
	{
		return SEMI;
	}
};

double gaussrand(double E, double sigma);

//this function should be rewrite. It is used to generate CNT pitch.
double pitch_Guass()
{
	double gap= gaussrand(parameter::pitch,parameter::pitch_deviation);
	while(gap<=0)
	{
		gap = gaussrand(parameter::pitch, parameter::pitch_deviation);
	}
	return gap;
	//return parameter::pitch;
};

double Random_U()   /* 产生一个0~1之间的随机数 */
{
  double f;
  f = (float)(rand() % 100);
 /* printf("%f\n",f); */
  return f/100;
}
//double length_Guass()
//{
//	int Lambda = 20, k = 0;
//    long double p = 1.0;
//    long double l=exp(-300.0);  /* 为了精度，才定义为long double的，exp(-Lambda)是接近0的小数*/
////	cout << "l = " << l <<endl;
//    while (p>=l)
//    {
//        double u = Random_U();
//        p *= u;
//        k++;
//    }
//	//double length = parameter::cnt_length;
//	double length = (k-1)*10;
////	cout << length <<endl;
//	return length;
//};
double length_Guass()
{
	double len = gaussrand(parameter::cnt_length+100,parameter::cnt_length_deviation);
	while(len<=0)
	{
		len = gaussrand(parameter::cnt_length+100,parameter::cnt_length_deviation);
	}
	if(len > parameter::cnt_length)
		return parameter::cnt_length;
	else
		return len;
//	return 200;
}
extern double PI;
double end_Guass(double start)
{
	long k=rand()%parameter::slope_ratio+1;

	double end;
	if(k<=1)
	{
		double tangle = gaussrand(0.01,40);
		while(tangle < 0 || tangle>85)
		{
			tangle = gaussrand(0.1,40);
		}
		double radian = tangle * PI / 180;
		//cout <<"tangle = " << tangle << " shift = "<< tan(radian)<<endl;
		end = start + parameter::cnt_length * tan(radian);
//		end = start;
	}
	else
	{
		end = start;
	}
	return end;
}
double starting_point_Guass(double &length,int &band)
{
	if(length >= parameter::cnt_length)
		return band * parameter::cnt_length;
	else
	{
		double displacement;
		displacement = parameter::cnt_length - length;
		displacement *= Random_U();
		return band * parameter::cnt_length+displacement;
	}	
}
class memory;
class test_result
{
public:
	test_result()
	{
		sram_tag = new bool*[parameter::bl_length];
		for(int i=0; i<parameter::bl_length; ++i)
		{
			sram_tag[i]=new bool[parameter::wl_length];
			for(int j=0;j<parameter::wl_length;++j)
				sram_tag[i][j] = true;
		}
		defect_sram_num = 0;
	}
	void add_access(int &row,int &col,int &operation)
	{
		Myresult tmp;
		tmp.row=row;
		tmp.column=col;
		tmp.operation=operation;
		m_result.push_back(tmp);
	}
	int get_tot_test_access()
	{
		return m_result.size();
	}
	Myresult get_access(int k)
	{
		return m_result[k];
	}
	void tag_sram(int row_begin,int row_end,int col)
	{
		for(int k=row_begin;k<=row_end;++k)
			sram_tag[k][col] = false;
	}
	bool get_sram_test(int row,int col)
	{
		return sram_tag[row][col];
	}
	void compute_sram_defects()
	{
		for(int row=0;row<parameter::wl_length;++row)
		{
			for(int col=0;col<parameter::bl_length;++col)
			{
				if(sram_tag[row][col]==false)
					++defect_sram_num;
			}
		}
	}

	void dump(memory &mem,string name,string path,bool rand_fault_map[][256]);
	void sample_dump(memory &mem,string name,string path,bool rand_fault_map[][256]);

	void dump(memory &mem,string name,bool rand_fault_map[][256]);
	void sample_dump(memory &mem,string name,bool rand_fault_map[][256]);

	long get_findout_sram_defects()
	{
		return defect_sram_num;
	}
	int get_test_cost()
	{
		return m_result.size();
	}
private:
	vector<Myresult> m_result;
	bool **sram_tag;
	long defect_sram_num;
};


class cnt
{
public:
	cnt()
	{
		m_attri=SEMI;
		m_length=0;
		m_position=0;
		m_end=0;
		m_band=0;
		double m_starting_point = 0;
	}
//	cnt(int& band,double& position,attri_t& attri,double& length)
//	{
//		m_band=band;
//		m_position=position;
//		//need to supplement
//		m_attri=attri;
//		m_length=length;
////		m_end = position;
//	}
	cnt(int& band,double& position,double& end,attri_t& attri,double& length,double &starting_point)
	{
		m_band=band;
		m_position=position;
		m_end=end;
		//need to supplement
		m_attri=attri;
		m_length=length;
		m_starting_point = starting_point;
	}
	attri_t get_attri()
	{
		return m_attri;
	}
	void set_attri(attri_t attri)
	{
		m_attri=attri;
	}
	double get_length()
	{
		return m_length;
	}
	void set_length(double length)
	{
		m_length=length;
	}
	
	double get_position()
	{
		return m_position;
	}
	/*void set_position(double position)
	{
		m_position=position;
	}*/
	double get_end()
	{
		return m_end;
	}
	bool is_straight()
	{
		return m_position==m_end?true:false;
	}
	double get_starting_point()
	{
		return m_starting_point;
	}
	int get_band()
	{
		return m_band;
	}
private:
	attri_t m_attri;
	double m_length;
	double m_position;
	double m_end;
	int m_band;
	//if a cnt is straight, the two parameters use to locate,relative coordinates
	double m_starting_point;
};

class transistor
{
public:
	transistor()
	{
		num_cnt=0;
		m_functional=true;
		length=0;
		width=0;
	}

	vector<cnt>::size_type find_first_cnt(vector<cnt>::size_type start,vector<cnt>::size_type end,int &num_stripe,vector< vector<cnt> > &_cnt,double &left_bottom_x,double &right_top_x);

	void init(int& row,int& column,vector< vector<cnt> >& _cnt,int& num_trans)
	{
		//genertare trans position, it should be rewrite
		double left_bottom_x = column * parameter::sram_width + num_trans / 2 * (parameter::trans_width + parameter::tran_width_pitch);
		double left_bottom_y = row * parameter::sram_height + num_trans % 2 * (parameter::trans_height + parameter::tran_height_pitch);
		double right_top_x = left_bottom_x + parameter::trans_width;
		double right_top_y = left_bottom_y + parameter::trans_height;

		
		/*cout << "row = " << row << " column = " << column << " num_trans = " << num_trans << endl;
		cout << "left_bottom_x = " << left_bottom_x 
			<< " left_bottom_y = " << left_bottom_y
			<< " right_top_x = " << right_top_x 
			<< " right_top_y = " << right_top_y << endl << endl;*/
		//find the first cnt in the transistor
		//it needs exception handling
		int num_stripe = right_top_y / parameter::cnt_length;
		//cout << "debug 0 " << num_stripe << " " << _cnt[num_stripe].size() - 1 << endl; // could be -1 and overflow here
		if (num_stripe >= _cnt.size())
		{
			cout << "exceed CNT stripe num!" << endl;
		}
		//search algorithm should be optimized
		//only consider m-CNT, so find_first_cnt do not used
		//cout << "debug 1 " << num_stripe << " " << _cnt[num_stripe].size() - 1 << endl;
		vector<cnt>::size_type first = find_first_cnt(0,_cnt[num_stripe].size()-1,num_stripe,_cnt,left_bottom_x,right_top_x);
		//if the program runs too slow when stuck_open_switch is open, try to set 'll = first' in the for loop in the next line. first is used like a binary search to find a better candidate.
		for(vector<cnt>::size_type ll=0; ll!=_cnt[num_stripe].size(); ++ll)
		{
			double pos = _cnt[num_stripe][ll].get_position();
			double cnt_end_x = _cnt[num_stripe][ll].get_end();
			if(pos < left_bottom_x && cnt_end_x<left_bottom_x)
			{
				continue;
			}
			else if(pos > right_top_x && cnt_end_x >right_top_x)
			{
				continue;
			}
			//else if((pos >= left_bottom_x) && (pos < right_top_x))
			else
			{
				//detect fault
				//it shoult be rewrite
				//cout <<"num_stripe = " << num_stripe <<" ll = " << ll << "starting_point"<<_cnt[num_stripe][ll].get_starting_point() << endl;
				if(_cnt[num_stripe][ll].get_attri() == SEMI)
				{
					if(_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y &&
						_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() > right_top_y)
					{
						++num_cnt;
					}
				}
				else if(_cnt[num_stripe][ll].get_attri() == METAL)
				{
					
					if(_cnt[num_stripe][ll].is_straight() == false)
					{
						
						double line_st_x = _cnt[num_stripe][ll].get_position();
						double line_st_y = _cnt[num_stripe][ll].get_starting_point();
						double line_end_x = _cnt[num_stripe][ll].get_end();
						double line_end_y = line_end_x + _cnt[num_stripe][ll].get_length();
						//bool intersect = isSectionTwo(line_st_x, line_st_y, line_end_x, line_end_y, left_bottom_x, left_bottom_y, right_top_x, right_top_y);
						//int intersect = isSectionTwo(4, 5, 10, 10, 6, 6, 7, 7);
						bool intersect = is_intersect_t(left_bottom_x, left_bottom_y, right_top_x, right_top_y,line_st_x, line_st_y, line_end_x, line_end_y );
						/*cout << "line_st_x = " <<line_st_x <<endl
							<< "line_st_y = " <<line_st_y <<endl
							<< "line_end_x = " <<line_end_x <<endl
							<< "line_end_y = " <<line_end_y <<endl
							<< "left_bottom_x = " <<left_bottom_x <<endl
							<< "left_bottom_y = " <<left_bottom_y <<endl
							<< "right_top_x = " <<right_top_x <<endl
							<< "right_top_y = " <<right_top_y <<endl;*/
						//cout << "intersect = " << intersect << endl <<endl;
						
						if(intersect == true)
						{
							m_functional = false;
							trans_defect = TRANS_SLOPE;
							
						}
						else
						{
							m_functional=true;
						}						;
					}
					else if(_cnt[num_stripe][ll].get_length() < parameter::cnt_length)
					{
						if(num_trans%2 == 0)
						{
							if((_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() > right_top_y) &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() < right_top_y + parameter::tran_height_pitch)
							{
								/*cout << "row = " << row << " column = " << column << " num_trans = " << num_trans
									<< " starting_point"<<_cnt[num_stripe][ll].get_starting_point() << endl;*/
								m_functional = false;
								trans_defect = TRANS_LENGTH;
							}
							else if((_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() > right_top_y) &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() >= right_top_y + parameter::tran_height_pitch +parameter::trans_height)
							{
								m_functional = false;
								trans_defect = TRANS_METAL;	
							}
							else
								m_functional = true;
						}
						else if(num_trans%2 == 1)
						{
							if((_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() > right_top_y) &&
								_cnt[num_stripe][ll].get_starting_point() > left_bottom_y - parameter::tran_height_pitch)
							
							{
								m_functional = false;
								trans_defect = TRANS_LENGTH;
							}
							else if((_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y &&
								_cnt[num_stripe][ll].get_starting_point() + _cnt[num_stripe][ll].get_length() > right_top_y) &&
								_cnt[num_stripe][ll].get_starting_point() <= left_bottom_y - parameter::tran_height_pitch - parameter::trans_height)
							{
								m_functional = false;
								trans_defect = TRANS_METAL;	
							}
							else
								m_functional = true;
						}
					}
					else
					{
						m_functional = false;
						trans_defect = TRANS_METAL;					
					}
//					cout << "row = " << row << " column = " << column << " num_trans = " << num_trans << endl;
//					cout << "ll = " << ll << " pos = " << pos << endl << endl;
				}
				/*else if(_cnt[num_stripe][ll].get_length() < parameter::cnt_length)
				{
					m_functional = false;
					trans_defect = TRANS_LENGTH;
				}
				else if(_cnt[num_stripe][ll].is_straight() == false)
				{
					m_functional = false;
					trans_defect = TRANS_SLOPE;
				}*/
			}
		}
		//set stuck-open status here. After traverse all the CNTs in one transistor and find no m-CNt and s-cnt, the transistor has stuck-open fault
		//if you run the program and find some problems, it may exist in the following code.
		if(parameter::stuck_open_switch ==1 && m_functional==true)
		{
			if(num_cnt == 0)
			{
				m_functional =false;
				//now regard stuck-open fault as the same fault caused by m-CNT, if we need accuracy it should be rewrite
				trans_defect = TRANS_METAL;
			}
		}
	}

	bool get_functional()
	{
		return m_functional;
	}

	int get_num_cnt()
	{
		return num_cnt;
	}

	trans_defect_t get_trans_defect()
	{
		return trans_defect;
	}

private:
	//vector<cnt> m_cnt;
	int num_cnt;
	bool m_functional;
	double length;
	double width;
	trans_defect_t trans_defect;
};

class sram
{
public:
	sram()
	{
		m_functional = false;
		defect = -1;
		m_delay = 1;
	}
	void set_defect(int& i)
	{
		//it only include metal-fault
		if(m_trans[i].get_trans_defect() == TRANS_METAL)
		{
			switch (i)
			{
			case 0:case 1:case 2:case 3:
				defect = STUCK_AT_1;break;
			case 4:case 5:
				defect = STUCK_AT_0;break;
			default:
				cout << "trans num = " << i << "exceed TRANS_PER_SRAM = " << TRANS_PER_SRAM << endl;
			}
		}
		else if(m_trans[i].get_trans_defect() == TRANS_LENGTH)
		{
			switch(i)
			{
			case 0:case 3:case 4:
				defect = STUCK_AT_1;break;
			case 1:case 2:case 5:
				defect = STUCK_AT_0;break;
			default:
				cout << "trans num = " << i << "exceed TRANS_PER_SRAM = " << TRANS_PER_SRAM << endl;
			}
		}
		else if(m_trans[i].get_trans_defect() == TRANS_SLOPE)
		{
			defect = DEFECT_SLOPE;
		}
		else
		{
			cout << "SRAM defect error!" << endl;
		}
	}
	void init(int& row,int& column,vector< vector<cnt> >& _cnt)
	{
		//cout << "row=" << row << ",column=" << column << endl;
		int pos_min_cnt = 0;
		for( int i=0;i<TRANS_PER_SRAM;i++)
		{
			//cout << "debug 5 " << _cnt[0].size() << endl;
			m_trans[i].init(row,column,_cnt,i);
			//used to compute delay
			/*if(m_trans[i].get_num_cnt()<m_trans[pos_min_cnt].get_num_cnt())
				pos_min_cnt = i;*/
		}
		double max_cnt = parameter::trans_width/parameter::pitch;
		if(pos_min_cnt > max_cnt)
			pos_min_cnt = max_cnt;
		m_delay = delay_hash[pos_min_cnt];
		m_functional=true;
		for( int i=0;i<TRANS_PER_SRAM;i++)
		{
			if(m_trans[i].get_functional()==false)
			{
				m_functional=false;
				set_defect(i);
				break;
			}
		}
		//trans_defect_to_sram_fault();
	}
	
	bool is_functional()
	{
		return m_functional;
	}
	void set_fault(fault_t &fault)
	{
		m_functional=false;
		defect=fault;
	}
	int get_defect()
	{
		return defect;
	}
	trans_defect_t get_trans_defect(int k)
	{
		return m_trans[k].get_trans_defect();
	}
	bool get_trans_functional(int k)
	{
		return m_trans[k].get_functional();
	}
	int get_sram_delay()
	{
		return m_delay;
	}
private:
	transistor m_trans[TRANS_PER_SRAM];
	bool m_functional;
	int defect;
	int m_delay;
	void trans_defect_to_sram_fault();

};

class memory
{
public:
	memory():m_height(0),m_width(0),_sram(NULL),wl_length(0),bl_length(0),num_cnt_rows(0),num_sram_rows(0),num_sram_columns(0),tot_sram_defects(0)
	{
		/*m_height = 0;
		m_width = 0;
		_sram=NULL;
		wl_length = 0;
		bl_length = 0;
		num_cnt_rows = 0;
		num_sram_rows = 0;
		num_sram_columns = 0;
		*/
	}

	memory(int& wl_length,int& bl_length,double& sram_height,double& sram_width,double& cnt_length,double& pitch):tot_sram_defects(0)
	{
		m_height=bl_length * sram_height;
		m_width=wl_length * sram_width;
		//its precision may be wrong, ceil to the integer?
		num_cnt_rows = ceil(m_height/cnt_length);
		//here is wrong, it has been exchange on 2014-10-27
		num_sram_rows = bl_length;
		num_sram_columns = wl_length;

		_sram = new sram*[num_sram_rows];
		for(int i=0; i<num_sram_rows; i++)
		{
			_sram[i]=new sram[num_sram_columns];
		}
		//cout << "m_height = " << m_height << endl << "m_width = " << m_width << endl << " num_cnt_rows = " << num_cnt_rows << endl
//			<< "num_sram_rows = " << num_sram_rows << endl <<"num_sram_columns = " << num_sram_columns << endl;
		
	}
	
	void init()
	{
		//init each row of CNT
		//it should be rewrite
		for(int i=0;i<num_cnt_rows;i++)
		{
			generate_cnt(i);
		}
		//cout << "CNT generation complete!" << endl;
		//cout << "******************************" << endl;
		/*cout << "_cnt size = "<<_cnt.size() << endl;*/
//		cout <<"size of _cnt = " <<sizeof(_cnt) << endl;
		//initialize sram
		for(int i=0; i<num_sram_rows; i++)
		{
			for(int j=0;j<num_sram_columns;j++)
			{
				//cout << "debug 4 " << _cnt[0].size() << endl;
				_sram[i][j].init(i,j,_cnt);
			}
		}
		//cout << "Memory initialization complete!" << endl;
		//cout << "******************************" << endl;
	}

	void generate_cnt(int& band)
	{
		vector<cnt> tmp_cnt;
		double intra_position = 0;				//tht position of the cnt in the stripe
		double pitch = pitch_Guass();
		while ( (intra_position + pitch) < m_width)
		{
			intra_position += pitch;
			attri_t cnt_attri = is_metal();
			double end = 0,length = 0,starting_point = 0;
			//cout << "cnt attribut = " << cnt_attri << endl;
			if(cnt_attri == METAL)
			{
				end = end_Guass(intra_position);
				if(end != intra_position)
				{
					length = length_Guass();
					starting_point = starting_point_Guass(length,band);
				}
				else
				{
					length = length_Guass();
					//ofstream output;
					/*output.open("CNT_Length_Sample.txt",ios::app);
					output << length << endl;
					output.close();*/
					if( length != parameter::cnt_length)
						starting_point = starting_point_Guass(length,band);
					else
						starting_point = band * parameter::cnt_length;
//					cout << "intra_position = " << intra_position << " length = " << length 
//						<< " starting_point endl = " << starting_point << endl;
				}
			}
			//暂不考虑s-CNT
			//add switch that can perform two options - consider stuck-open and do not consider
			else
			{
				length = length_Guass();
				end = intra_position;
			//	//relative coordinates
				if(length != parameter::cnt_length)
					starting_point = starting_point_Guass(length,band);
				else
					starting_point = band * parameter::cnt_length;
			}
			//		cnt c = cnt(band,intra_position,cnt_attri,length);
			cnt c = cnt(band,intra_position,end,cnt_attri,length,starting_point);
			if(cnt_attri == METAL || parameter::stuck_open_switch == 1)
			{
				tmp_cnt.push_back(c);
			}
			pitch = pitch_Guass();
		}
		_cnt.push_back(tmp_cnt);
	}
	void print_mem(BipGraph *graph, bool rand_fault_map[256][256], int blk)
	{
        /*
		ofstream output;
		output.open("Mem_parameter.txt",ios::out);
		output << "Start output memory initialization parameter......" << endl;
		time_t t;
		time ( &t );
		output << "The current date/time is: "<<ctime(&t);

		output << "************************************************************" << endl;
		output << "wl_length = "<< parameter::wl_length << endl
		<< "bl_length = "<< parameter::bl_length <<endl 
		<< "sram_height = "<< parameter::sram_height << endl
		<< "sram_width = "<< parameter::sram_width << endl
		<< "cnt_length = "<< parameter::cnt_length << endl
		<< "pitch = " << parameter::pitch << endl
		<< "trans width = " << parameter::trans_width << endl
		<< "trans height = " << parameter::trans_height << endl;

		output << "************************************************************" << endl;
		output << "Output Sram initialization parameter......" << endl;*/
#ifdef IMAGE
		CNTImage image(num_sram_rows,num_sram_columns,8);
#endif
		for(int i=0; i<num_sram_rows; i++)
		{
			//output << "row = " << setw(7) << i;
			for(int j=0;j<num_sram_columns;j++)
			{
				if(_sram[i][j].is_functional() == false)
				{
                    graph->addEdge(i,j);
                    rand_fault_map[i][j] = true;
				/*	++tot_sram_defects;
					switch(_sram[i][j].get_defect())
					{
						case 0:output << setw(0) << "0";break;
						case 1:output << "1";break;
						case 2:output << "L";break;
						case 3:output << "S";break;
						default: output << "WRONGSRAM";
					}*/ 
#ifdef IMAGE
					image.setCell(i,j,new Pixel(255, 0, 0)); 
#endif
				}
				/*else
				{
					output << setw(0) << "-";
				}*/
			}/*
			output << endl;

			int tmp = parameter::cnt_length / parameter::sram_height;
			if (i%tmp == tmp-1)
			{
				output << endl;
			}*/
		}
        
#ifdef IMAGE
		int blksize = ((num_sram_rows-1)/blk) + 1;
		for (int i = 1; i < blk; ++i)
			image.setLine(i * blksize - 1, new Pixel(0, 0, 255));

        int blk1 = blk + 1;
        int blksize1 = ((num_sram_rows-1)/blk1) + 1;
        for (int i = 1; i< blk1; ++i)
            image.setLine(i * blksize1 - 1, new Pixel(0, 255, 0));

		image.printImage("res.png");
#endif
		/*output << "tot_sram_defects = " << tot_sram_defects << endl;
		output << "tot_sram_cells = " << (parameter::bl_length * parameter::wl_length) << endl;
		output << "tot_sram_defects_ratio = " << ((double)tot_sram_defects)/(parameter::bl_length * parameter::wl_length) << endl;
		output.close();*/
	}
	bool get_sram_functional(int row,int column);
	int get_sram_fault(int row,int column);
	int get_stripe_num()
	{
		return _cnt.size();
	}
	long get_tot_sram_defects()
	{
		return tot_sram_defects;
	}
	void rand_fault_injection(int row,int col,fault_t &fault)
	{
		_sram[row][col].set_fault(fault);
	}
	~memory()
	{
		
		for(int i=0; i<num_sram_rows; i++)
		{
//			cout << "i = " << i<<endl;
			delete [] _sram[i];
		}
		delete _sram;
	}
private:
	double m_height;
	double m_width;
	vector< vector<cnt> > _cnt;
	sram **_sram;
	int wl_length;
	int bl_length;
	int num_cnt_rows;
	int num_sram_rows;
	int num_sram_columns;
	long tot_sram_defects;
};

bool access(memory &mem, sram_status_t **sram_status, int &row, int col, string &operation,int &k,test_result& result)
{
	result.add_access(row,col,k);
	if(operation == "R0")
	{
		if(mem.get_sram_fault(row,col)==DEFECT_SLOPE)
			return false;
		else if(sram_status[row][col].readout == 0)
			return 1;
		else
			return 0;
	}
	else if(operation == "R1")
	{
		if(mem.get_sram_fault(row,col)==DEFECT_SLOPE)
			return false;
		else if(sram_status[row][col].readout == 1)
			return 1;
		else
			return 0;
	}
	else
	{
		if(operation == "W1")
			sram_status[row][col].store = 1;
		else if(operation == "W0")
			sram_status[row][col].store = 0;
		if(mem.get_sram_functional(row,col) == true)
		{
			if(operation == "W1")
				sram_status[row][col].readout = 1;
			else if(operation == "W0")
				sram_status[row][col].readout = 0;

		}
		else
		{
			if(mem.get_sram_fault(row,col) == STUCK_AT_1)
				sram_status[row][col].readout = 1;
			else if(mem.get_sram_fault(row,col) == STUCK_AT_0)
				sram_status[row][col].readout = 0;
		}
		return 1;
	}
}

int locate(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	if( w-row <= parameter::test_step)
		return -1;
	int mid = (row + w)/2;
	int u = k-1;
	access(mem,sram_status,mid,col,algorithm[u],u,result);//it shoult be written before read
	bool tag_mid = access(mem,sram_status,mid,col,algorithm[k],k,result);
	if(tag_mid == false)
		return mid;
	else
	{
		int tmp = locate(mem,sram_status,row,mid-1,col,algorithm,k,result);
		if(tmp != -1)
			return tmp;
		else
			return locate(mem,sram_status,mid+1,w,col,algorithm,k,result);
	}
}
int find_start_pos(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int k,test_result& result,int pos_fault)
{
	int find_st=row;
	int find_end=pos_fault;
	int find_mid = (find_end + find_st)/2;
	bool tag_mid;
	while( find_end-find_st >parameter::test_step)
	{
		int u=k-1;
		access(mem,sram_status,find_mid,col,algorithm[u],u,result);//it shoult be written before read
		tag_mid = access(mem,sram_status,find_mid,col,algorithm[k],k,result);
		if(tag_mid == true)
		{
			find_st=find_mid+1;
			find_mid = (find_end + find_st)/2;
		}
		else if(tag_mid == false)
		{
			find_end=find_mid;
			find_mid = (find_end + find_st)/2;
		}
	}
	while(find_st < find_end)
	{
		int u=k-1;
		access(mem,sram_status,find_st,col,algorithm[u],u,result);//it shoult be written before read
		if(access(mem,sram_status,find_st,col,algorithm[k],k,result) == false)
			break;
		++find_st;
	}
	return find_st;
}

int find_end_pos(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int k,test_result& result,int pos_fault)
{
	int find_st=pos_fault;
	int find_end=w;
	int find_mid = (find_end + find_st)/2;
	bool tag_mid;
	while( find_end-find_st >parameter::test_step)
	{
		int u=k-1;
		access(mem,sram_status,find_mid,col,algorithm[u],u,result);//it shoult be written before read
		tag_mid = access(mem,sram_status,find_mid,col,algorithm[k],k,result);
		/*cout << "find_mid = " << find_mid << " tag_mid = " << tag_mid << " k = " << k << endl;*/
		if(tag_mid == true)
		{
			find_end=find_mid-1;
			find_mid = (find_end + find_st)/2;
		}
		else if(tag_mid == false)
		{
			find_st=find_mid;
			find_mid = (find_end + find_st)/2;
		}
	}
	//cout<< "find_st = " << find_st <<" find_end = " << find_end << endl;
	while(find_st < find_end)
	{
		int u=k-1;
		access(mem,sram_status,find_end,col,algorithm[u],u,result);//it shoult be written before read
		/*cout << "u = " << find_end << "u = " <<tag_tmp << " k = " << k << endl;
		cout << "find_end = " << find_end << "tmp1 = " <<tag_tmp << endl;*/
		if(access(mem,sram_status,find_end,col,algorithm[k],k,result) == false)
			break;
		--find_end;
	}
	return find_end;
}

int Jump_locate(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	int u=k-1;
	access(mem,sram_status,row,col,algorithm[u],u,result);//it shoult be written before read
	bool tag = access(mem,sram_status,row,col,algorithm[k],k,result);
	if(tag == false)
		return row;
	access(mem,sram_status,w,col,algorithm[u],u,result);//it shoult be written before read
	tag = access(mem,sram_status,w,col,algorithm[k],k,result);
	if(tag == false)
		return w;
	add_pair_t tmp;
	stack<add_pair_t> add_partition;
	queue<add_pair_t> tmp_partition;
	tmp.high = w;
	tmp.low = row;
	add_partition.push(tmp);
	while(add_partition.empty() != true || tmp_partition.empty() != true)
	{
//		cout << "come in cycle" << endl;
		int mid;
		tmp.high = add_partition.top().high;
		tmp.low = add_partition.top().low;
		add_partition.pop();
		if(tmp.high - tmp.low > parameter::test_step)
		{
//			cout << "come in judge "<< endl;
			mid = (tmp.high + tmp.low) /2;
			access(mem,sram_status,mid,col,algorithm[u],u,result);//it shoult be written before read
			bool tag_mid = access(mem,sram_status,mid,col,algorithm[k],k,result);
			if(tag_mid == false)
				return mid;
			else
			{
				add_pair_t tmp2;
				tmp2.low = tmp.low;
				tmp2.high = mid;
				//the first set enqueue
				tmp_partition.push(tmp2);
				tmp2.low = mid;
				tmp2.high = tmp.high;
				//the second set enqueue
				tmp_partition.push(tmp2);
			}
			if(add_partition.empty() == true)
			{
				while(tmp_partition.empty() != true)
				{
					tmp.high = tmp_partition.front().high;
					tmp.low = tmp_partition.front().low;
					add_partition.push(tmp);
					tmp_partition.pop();
				}
			}
		}
	}
	return -1;
}


void test_metal(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;++col)
	{
		for(int k=0;k<4;++k)
		{
			for(int row=0;row<parameter::bl_length;row+=sram_per_stripe)
			{
				bool tag_low = access(mem,sram_status,row,col,algorithm[k],k,result);
				int w = row+sram_per_stripe-1;
				if(w >= parameter::bl_length)
					w = parameter::bl_length-1;
				bool tag_high = access(mem,sram_status,w,col,algorithm[k],k,result);
//**************************************************************************************
				if(tag_low == true && tag_high == true && (algorithm[k] == "R1" || algorithm[k] == "R0") )
				{
					int pos_fault = locate(mem,sram_status,row,w,col,algorithm,k,result);
					/*cout << "row_low = " << row << " row_high = " << w << "column = " << col << endl;
					cout << "pos = " << pos_fault << " operation = " << algorithm[k] <<endl << endl;*/
					if(pos_fault != -1)
					{
						int start_pos = find_start_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
						int end_pos = find_end_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
						result.tag_sram(start_pos,end_pos,col);
					}
				}
//**************************************************************************************
				else if(tag_low == false && tag_high == false)
				{
					static int compute = 0;
//					cout << "come in = " << ++compute << endl;
//					cout << "row = " << row <<" w = " << w << " col = " << col << " operation = " << algorithm[k] <<endl;
					result.tag_sram(row,w,col);
				}
//**************************************************************************************
				else if(tag_low == true &&tag_high == false)
				{
					int find_st=row;
					int find_end=w;
					int find_mid = (find_end + find_st)/2;
					bool tag_mid;
					while( find_end-find_st > parameter::test_step)
					{
						int u=k-1;
						access(mem,sram_status,find_mid,col,algorithm[u],u,result);//it shoult be written before read
						tag_mid = access(mem,sram_status,find_mid,col,algorithm[k],k,result);
						if(tag_mid == true)
						{
							find_st=find_mid+1;
							find_mid = (find_end + find_st)/2;
						}
						else if(tag_mid == false)
						{
							find_end=find_mid;
							find_mid = (find_end + find_st)/2;
						}
					}
					while(find_st < find_end)
					{
						int u=k-1;
						access(mem,sram_status,find_st,col,algorithm[u],u,result);//it shoult be written before read
						if(access(mem,sram_status,find_st,col,algorithm[k],k,result) == false)
							break;
						++find_st;
					}
					result.tag_sram(find_st,w,col);
					/*cout << "row_low = " << row << "column = " << col << endl;
					cout << "row_high = " << w << "column = " << col << endl;
					cout << "bad case!" << endl << endl;*/
				}
//**************************************************************************************
				else if(tag_low == false &&tag_high == true)
				{
					int find_st=row;
					int find_end=w;
					int find_mid = (find_end + find_st)/2;
					bool tag_mid;
					while( find_end-find_st >parameter::test_step)
					{
						int u=k-1;
						access(mem,sram_status,find_mid,col,algorithm[u],u,result);//it shoult be written before read
						tag_mid = access(mem,sram_status,find_mid,col,algorithm[k],k,result);
						/*cout << "find_mid = " << find_mid << " tag_mid = " << tag_mid << " k = " << k << endl;*/
						if(tag_mid == true)
						{
							find_end=find_mid-1;
							find_mid = (find_end + find_st)/2;
						}
						else if(tag_mid == false)
						{
							find_st=find_mid;
							find_mid = (find_end + find_st)/2;
						}
					}
					//cout<< "find_st = " << find_st <<" find_end = " << find_end << endl;
					while(find_st < find_end)
					{
						int u=k-1;
						access(mem,sram_status,find_end,col,algorithm[u],u,result);//it shoult be written before read
						/*cout << "u = " << find_end << "u = " <<tag_tmp << " k = " << k << endl;
						cout << "find_end = " << find_end << "tmp1 = " <<tag_tmp << endl;*/
						if(access(mem,sram_status,find_end,col,algorithm[k],k,result) == false)
							break;
						--find_end;
					}
					/*cout << "lable ... row = " << row << " find_end = " << find_end << endl;*/
					result.tag_sram(row,find_end,col);
					/*cout << "row_low = " << row << " column = " << col << endl;
					cout << "row_high = " << w << " column = " << col << endl;
					cout << "bad case222!" << endl << endl;*/
				}
			}
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}
void test_metal_Jump(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;++col)
	{
		for(int k=1;k<4;k+=2)
		{
			for(int row=0;row<parameter::bl_length;row+=sram_per_stripe)
			{
				int w = row+sram_per_stripe-1;
				if(w >= parameter::bl_length)
					w = parameter::bl_length-1;
				int pos_fault=Jump_locate(mem,sram_status,row,w,col,algorithm,k,result);
//				cout << "pos_fault = " << pos_fault << endl;
				if(pos_fault != -1)
				{
					int start_pos = find_start_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
					int end_pos = find_end_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
					result.tag_sram(start_pos,end_pos,col);
				}
			}
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}

void test_metal_delay_Jump(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;++col)
	{
		for(int k=1;k<4;k+=2)
		{
			for(int row=0;row<parameter::bl_length;row+=sram_per_stripe)
			{
				int w = row+sram_per_stripe-1;
				if(w >= parameter::bl_length)
					w = parameter::bl_length-1;
				int pos_fault=Jump_locate(mem,sram_status,row,w,col,algorithm,k,result);
//				cout << "pos_fault = " << pos_fault << endl;
				if(pos_fault != -1)
				{
					int start_pos = find_start_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
					int end_pos = find_end_pos(mem,sram_status,row,w,col,algorithm,k,result,pos_fault);
					result.tag_sram(start_pos,end_pos,col);
				}
			}
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}

//****************************************************************************************************
//Follows are Word-oriented test

//*****************************************************************************************
//void Word_jump_locate(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
//{
//
//	add_pair_t tmp;
//	stack<add_pair_t> add_partition;
//	queue<add_pair_t> tmp_partition;
//	int u=k-1;
//	for(int test_row=row;test_row<=w;test_row+=parameter::initialization_test_step)
//	{
//		bool flag_word = true;
//		for(int i=0;i<parameter::word_width;++i)
//		{
//			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
//			bool tag = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
//			tmp.high = test_row+parameter::initialization_test_step;
//			tmp.low = test_row-parameter::initialization_test_step;
//			if(parameter::initialization_test_step/parameter::test_step<2)
//			{
//				if(tmp.high>w)
//					tmp.high=w;
//				if(tmp.low<row)
//					tmp.low=row;
//				if(tag == false)
//				{
//					flag_word = false;
//					if(tmp.high==w &&tmp.low == row)
//						result.tag_sram(tmp.low,tmp.high,col+i);
//					else if(tmp.high!=w && tmp.low!=row)
//						result.tag_sram(tmp.low+1,tmp.high-1,col+i);
//					else if(tmp.low==row)
//						result.tag_sram(row,tmp.high-1,col+i);
//					else if(tmp.high==w)
//						result.tag_sram(tmp.low+1,tmp.high,col+i);
//				}
//				else
//				{
//				}
//			}
//			else
//			{
//				if(tmp.high>w)
//					tmp.high=w;
//				if(tmp.low<row)
//					tmp.low=row;
//				if(tag == false)
//				{
//					result.tag_sram(test_row,test_row,col+i);
//				}
//			}
//		}
//		if (flag_word == true)
//		{
//			tmp.low = test_row;
//			if(tmp.low != w)
//				add_partition.push(tmp);
//		}
//	}
//	int mid;
////	cout<< "**************************************************************************************"<<endl;
//	while(add_partition.empty() != true)
//	{
////		cout << "come in cycle" << endl;
////		cout << "stack empty = " << add_partition.empty() << " stack size = " << add_partition.size() 
////			<< " queue empty = " << tmp_partition.empty() << " queue size = " << tmp_partition.size() << endl;
//
//		tmp.high = add_partition.top().high;
//		tmp.low = add_partition.top().low;
//		//cout << "tmp.high = " <<tmp.high << "tmp.low = " <<tmp.low << endl;
//		add_partition.pop();
//		bool flag_word = true;
//		if(tmp.high - tmp.low > parameter::test_step)
//		{
//			flag_word = true;
////			cout << "come in judge "<< endl;
//			mid = (tmp.high + tmp.low) /2;
//			for(int i=0;i<parameter::word_width;++i)
//			{
//				access(mem,sram_status,mid,col+i,algorithm[u],u,result);//it shoult be written before read
//				bool tag_mid = access(mem,sram_status,mid,col+i,algorithm[k],k,result);
//				if(tag_mid == false)
//				{
//					if(tmp.high != w)
//					{
//						result.tag_sram(tmp.low+1,tmp.high-1,col+i);
//					}
//					else
//					{
//						result.tag_sram(tmp.low+1,tmp.high,col+i);
//					}
//					flag_word = false;
//				}
//				else
//				{
//				}
//			}
//		}
//		if(flag_word == true && tmp.high - tmp.low > parameter::test_step)
//		{
//			add_pair_t tmp2;
//			tmp2.low = tmp.low;
//			tmp2.high = mid;
//			//the first set enqueue
//			tmp_partition.push(tmp2);
//			tmp2.low = mid;
//			tmp2.high = tmp.high;
//			//the second set enqueue
//			tmp_partition.push(tmp2);
//		}
//		if(add_partition.empty() == true)
//		{
//			while(tmp_partition.empty() != true)
//			{
//				tmp.high = tmp_partition.front().high;
//				tmp.low = tmp_partition.front().low;
//				add_partition.push(tmp);
//				tmp_partition.pop();
//			}
//		}
//	}
//
////*******************************************************************************************
////following used to handle the boundary condition
//
////	for(int test_row=row;test_row<parameter::test_step*2+row;test_row++)
////	for(int test_row=row;test_row<8+row;test_row++)
////	{
////		for(int i=0;i<parameter::word_width;++i)
////		{
////			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
////			bool tag = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
////			if(tag == false)
////				result.tag_sram(row,row+parameter::test_step-1,col+i);
////		}
////	}
//////	for(int test_row=w-parameter::test_step*2+1;test_row<=w;test_row++)
////	for(int test_row=w-8+1;test_row<=w;test_row++)
////	{
////		for(int i=0;i<parameter::word_width;++i)
////		{
////			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
////			bool tag = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
////			if(tag==false)
////				result.tag_sram(test_row,w,col+i);
////		}
////	}
//}

void locate_word_bisection(memory &mem,sram_status_t **sram_status,bool tags[][256],int st_row,int en_row,int col,string *algorithm,int k,test_result& result)
{
	int u=k-1;
	int start_row=st_row;
	int end_row=en_row;
	if(end_row-start_row<=parameter::test_step)
		return;
	int mid = (start_row+end_row) / 2;
	bool tag = true;
	for(int i=0;i<parameter::word_width;++i)
	{
		access(mem,sram_status,mid,col+i,algorithm[u],u,result);//it shoult be written before read
		tags[mid][col+i] = access(mem,sram_status,mid,col+i,algorithm[k],k,result);
		if(tags[mid][col+i]==false)
		{
			tag=false;
			result.tag_sram(start_row,end_row,col+i);
		}
	}
	if(tag==true)
	{
		locate_word_bisection(mem,sram_status,tags,start_row,mid,col,algorithm,k,result);
		locate_word_bisection(mem,sram_status,tags,mid,end_row,col,algorithm,k,result);
	}
}
void Word_jump_locate(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	int u=k-1;
	bool tags[256][256];

	//initalize mark table
	for(int i=0;i<parameter::bl_length;i++)
		for(int j=0;j<parameter::wl_length;j++)
			tags[i][j]=true;

	//first interation partitation memory
	for(int test_row=row;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		for(int i=0;i<parameter::word_width;++i)
		{
			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
			tags[test_row][col+i] = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
		}

		//next iteration to test the last word in one column
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
	}



	int test_row = row+parameter::initialization_test_step;
	if(test_row > w)
		test_row = w;
	int pre_row=row;
	for(;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		//cout<<"Coming--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		//original code in detect_traverse
		bool metallic=false;
		for(int i=0;i<parameter::word_width;i++)
		{
			int test_col=col+i;
			if(tags[test_row][test_col] == false || tags[pre_row][test_col] == false)
			{
				result.tag_sram(pre_row,test_row,test_col);
				metallic=true;
			}
			else if(tags[test_row][test_col]==false)
			{
				metallic = true;
				result.tag_sram(pre_row+1,test_row,test_col);
			}
			else if(tags[pre_row][test_col]==false)
			{
				metallic=true;
				result.tag_sram(pre_row,test_row-1,test_col);

			}
		}
		if(metallic==false)
		{
			locate_word_bisection(mem,sram_status,tags,pre_row,test_row,col,algorithm,k,result);
		}
		pre_row=test_row;
		//cout<<"Before--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
		//cout<<"After--pre_row = "<< pre_row<<" test_row = " << test_row << endl<< endl;
	}
}


void word_test_metal_Jump(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;col+= parameter::word_width)
	{
		for(int k=1;k<4;k+=2)   
		{
			int w = parameter::bl_length-1;
			Word_jump_locate(mem,sram_status,0,w,col,algorithm,k,result);
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}


//*****************************************************************************************
//following is word-oriented test,  deep recursion.

void detect_recursion(memory &mem,sram_status_t **sram_status,bool tags[][256],int st_row,int en_row,int col,string *algorithm,int k,test_result& result)
{
	int u=k-1;
	int start_row=st_row;
	int end_row=en_row;
	bool tag_high=false;
	bool tag_low=false;
	if(end_row-start_row <= parameter::test_step)
	{
		for(int test_row=start_row;test_row<=end_row;++test_row)
		{
			bool terminate = true;
			for(int i=0;i<parameter::word_width;i++)
			{
				int test_col=col+i;
				access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
				tags[test_row][test_col] = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
				/*if(tags[end_row][test_col]==false &&tags[test_row][test_col]==false)
					result.tag_sram(test_row,end_row,test_col);
				else if(tags[end_row][test_col]==false &&tags[test_row][test_col]==true)
					terminate=false;
				else if(tags[start_row][test_col]==false &&tags[test_row][test_col]==false)
					result.tag_sram(start_row,end_row,test_col);
				else if (tags[start_row][test_col]==false &&tags[test_row][test_col]==true)
					continue;*/
				if(tags[test_row][test_col]==false)
					result.tag_sram(test_row,test_row,test_col);
			}
			//if(terminate == true)
			//	return;
		}
		return;
	}
	int mid =(start_row+end_row)/2;
	for(int i=0;i<parameter::word_width;i++)
	{
		int test_col=col+i;
		access(mem,sram_status,mid,col+i,algorithm[u],u,result);//it shoult be written before read
		tags[mid][col+i] = access(mem,sram_status,mid,col+i,algorithm[k],k,result);
	}
	
	for(int i=0;i<parameter::word_width;i++)
	{
		int test_col=col+i;
		if(tags[start_row][test_col] == false && tags[end_row][test_col] == false)
		{
			result.tag_sram(start_row,end_row,test_col);
		}
		else if(tags[mid][test_col]==false)
		{
			if(tags[start_row][test_col] == true && tags[end_row][test_col] == true)
			{
				continue;
			}
			else if(tags[start_row][test_col] == false)
			{
				result.tag_sram(start_row,mid,test_col);
				tag_high=true;
			}
			else if(tags[end_row][test_col] == false)
			{
				tag_low=true;
				result.tag_sram(mid,end_row,test_col);
			}
			else if(tags[start_row][test_col]==true)
			{
				tag_low=true;
				result.tag_sram(mid,mid,test_col);
			}
			else if(tags[end_row][test_col]==true)
			{
				tag_high=true;
				result.tag_sram(mid,mid,test_col);
			}
		}
		else if(tags[mid][test_col] == true)
		{
			if(tags[start_row][test_col] == true && tags[end_row][test_col] == true)
			{
				continue;
			}
			else if(tags[start_row][test_col] == false)
			{
				tag_low=true;
			}
			else if(tags[end_row][test_col] == false)
			{
				tag_high=true;
			}
		}
	}
	if(tag_low==true)
	{
		detect_recursion(mem,sram_status,tags,start_row,mid,col,algorithm,k,result);
	}
	if(tag_high==true)
		detect_recursion(mem,sram_status,tags,mid,end_row,col,algorithm,k,result);
}

void locate_word_deep(memory &mem,sram_status_t **sram_status,bool tags[][256],int st_row,int en_row,int col,string *algorithm,int k,test_result& result)
{
	int u=k-1;
	int start_row=st_row;
	int end_row=en_row;
	if(end_row-start_row<=parameter::test_step)
		return;
	int mid = (start_row+end_row) / 2;
	bool tag = true;
	for(int i=0;i<parameter::word_width;++i)
	{
		access(mem,sram_status,mid,col+i,algorithm[u],u,result);//it shoult be written before read
		tags[mid][col+i] = access(mem,sram_status,mid,col+i,algorithm[k],k,result);
		if(tags[mid][col+i]==false)
		{
			tag=false;
			result.tag_sram(mid,mid,col+i);
		}
	}
	if(tag==true)
	{
		locate_word_deep(mem,sram_status,tags,start_row,mid,col,algorithm,k,result);
		locate_word_deep(mem,sram_status,tags,mid,end_row,col,algorithm,k,result);
	}
	else if(tag==false)
	{
		detect_recursion(mem,sram_status,tags,start_row,mid,col,algorithm,k,result);
		detect_recursion(mem,sram_status,tags,mid,end_row,col,algorithm,k,result);
	}
}

//following is word recursive jump
void Word_deep_jump_locate(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	int u=k-1;
	bool tags[256][256];

	//initalize mark table
	for(int i=0;i<parameter::bl_length;i++)
		for(int j=0;j<parameter::wl_length;j++)
			tags[i][j]=true;

	//first interation partitation memory
	for(int test_row=row;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		for(int i=0;i<parameter::word_width;++i)
		{
			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
			tags[test_row][col+i] = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
		}

		//next iteration to test the last word in one column
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
	}



	int test_row = row+parameter::initialization_test_step;
	if(test_row > w)
		test_row = w;
	int pre_row=row;
	for(;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		//cout<<"Coming--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		//original code in detect_traverse
		bool tag_locate=true;
		bool metallic=false;
		for(int i=0;i<parameter::word_width;i++)
		{

			int test_col=col+i;
			if(tags[test_row][test_col] == false && tags[pre_row][test_col] == false)
			{
				result.tag_sram(pre_row,test_row,test_col);
				metallic=true;
			}
			else if(tags[test_row][test_col]==false)
			{
					tag_locate=false;
					metallic = true;
				result.tag_sram(test_row,test_row,test_col);
			}
			else if(tags[pre_row][test_col]==false)
			{
				tag_locate=false;
				metallic=true;
				result.tag_sram(pre_row,pre_row,test_col);

			}
			else if (tags[test_row][test_col] == true && tags[pre_row][test_col] == true)
			{
				
			}
		}
		if(tag_locate==true&&metallic==false)
		{
			locate_word_deep(mem,sram_status,tags,pre_row,test_row,col,algorithm,k,result);
		}
		else if(tag_locate==true&&metallic==true)
		{
			//continue;
		}
		else if(tag_locate==false&&metallic==false)
		{
			cout <<"wrong test algorithm!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
		}
		else if(tag_locate==false&&metallic==true)
		{
			detect_recursion(mem,sram_status,tags,pre_row,test_row,col,algorithm,k,result);
		}
		pre_row=test_row;
		//cout<<"Before--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
		//cout<<"After--pre_row = "<< pre_row<<" test_row = " << test_row << endl<< endl;
	}
}


void word_test_metal_recursive_Jump(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;col+= parameter::word_width)
	{
		for(int k=1;k<4;k+=2)   
		{
			int w = parameter::bl_length-1;
			Word_deep_jump_locate(mem,sram_status,0,w,col,algorithm,k,result);
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}

//*****************************************************************************************
//following is static jump step test,no variation
void word_static_step(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	int u=k-1;
	bool tags[256][256];

	//initalize mark table
	for(int i=0;i<parameter::bl_length;i++)
		for(int j=0;j<parameter::wl_length;j++)
			tags[i][j]=true;

	//first interation partitation memory
	for(int test_row=row;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		for(int i=0;i<parameter::word_width;++i)
		{
			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
			tags[test_row][col+i] = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
		}

		//next iteration to test the last word in one column
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
	}

	int test_row = row+parameter::initialization_test_step;
	if(test_row > w)
		test_row = w;
	int pre_row=row;
	for(;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		//cout<<"Coming--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		//original code in detect_traverse
		
		for(int i=0;i<parameter::word_width;i++)
		{
			int test_col=col+i;
			if(tags[test_row][test_col] == false && tags[pre_row][test_col] == false)
			{
				result.tag_sram(pre_row,test_row,test_col);
			}
			else if(tags[test_row][test_col]==false)
			{
				//result.tag_sram(pre_row+1,test_row,test_col);
				result.tag_sram(test_row,test_row,test_col);
			}
			else if(tags[pre_row][test_col]==false)
			{
				//result.tag_sram(pre_row,test_row-1,test_col);
				result.tag_sram(pre_row,pre_row,test_col);

			}
			else if (tags[test_row][test_col] == true && tags[pre_row][test_col] == true)
			{
				
			}
		}
		
		pre_row=test_row;
		//cout<<"Before--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
		//cout<<"After--pre_row = "<< pre_row<<" test_row = " << test_row << endl<< endl;
	}
}
void word_test_staict_step(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int col=0;col<parameter::wl_length;col+= parameter::word_width)
	{
		for(int k=1;k<4;k+=2)   
		{
			int w = parameter::bl_length-1;
			word_static_step(mem,sram_status,0,w,col,algorithm,k,result);
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}
//*****************************************************************************************
void word_line_step(memory &mem,sram_status_t **sram_status,int row,int w,int col,string *algorithm,int &k,test_result& result)
{
	int u=k-1;
	bool tags[256][256];

	//initalize mark table
	for(int i=0;i<parameter::bl_length;i++)
		for(int j=0;j<parameter::wl_length;j++)
			tags[i][j]=true;

	//first interation partitation memory
	for(int test_row=row;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		for(int i=0;i<parameter::word_width;++i)
		{
			access(mem,sram_status,test_row,col+i,algorithm[u],u,result);//it shoult be written before read
			tags[test_row][col+i] = access(mem,sram_status,test_row,col+i,algorithm[k],k,result);
		}

		//next iteration to test the last word in one column
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
	}

	int test_row = row+parameter::initialization_test_step;
	if(test_row > w)
		test_row = w;
	int pre_row=row;
	for(;test_row<=w;test_row+=parameter::initialization_test_step)
	{
		//cout<<"Coming--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		//original code in detect_traverse
		
		for(int i=0;i<parameter::word_width;i++)
		{
			int test_col=col+i;
			if(tags[test_row][test_col] == false && tags[pre_row][test_col] == false)
			{
				result.tag_sram(pre_row,test_row,test_col);
			}
			else if(tags[test_row][test_col]==false)
			{
				result.tag_sram(pre_row+1,test_row,test_col);
			}
			else if(tags[pre_row][test_col]==false)
			{
				result.tag_sram(pre_row,test_row-1,test_col);

			}
			else if (tags[test_row][test_col] == true && tags[pre_row][test_col] == true)
			{
				
			}
		}
		
		pre_row=test_row;
		//cout<<"Before--pre_row = "<< pre_row<<" test_row = " << test_row << endl;
		if(test_row+parameter::initialization_test_step>w && test_row != w)
		{
			test_row = w - parameter::initialization_test_step;
		}
		//cout<<"After--pre_row = "<< pre_row<<" test_row = " << test_row << endl<< endl;
	}
}
void word_line_test(memory &mem,test_result& result)
{
	string algorithm[4] = {"W0","R0","W1","R1"};
	sram_status = new sram_status_t*[parameter::bl_length];
	for(int i=0; i<parameter::bl_length; i++)
	{
		sram_status[i]=new sram_status_t[parameter::wl_length];
	}
	int sram_per_stripe = parameter::cnt_length / parameter::sram_height;
	int stripe_num = mem.get_stripe_num();
	for(int row=0;row<parameter::bl_length;row++)
	{
		for(int k=1;k<4;k+=2)   
		{
			int w = parameter::wl_length-1;
			word_line_step(mem,sram_status,0,w,row,algorithm,k,result);
		}
	}
	result.compute_sram_defects();
	for(int i=0;i<parameter::bl_length;++i)
		delete [] sram_status[i];
	delete sram_status;
}

#endif
