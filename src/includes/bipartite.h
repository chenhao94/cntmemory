#ifndef __BIPARTITE_H__
#define __BIPARTITE_H__
#include <iostream>
#include <cstring>
#include <vector>

class BipGraph
{
    struct Edge
    {
        int dest, nextEdge;
        Edge(int d, int n):dest(d), nextEdge(n){}
    };
	//bool *visited;
    int *lhead, *rhead;
    std::vector<Edge> edge;
    std::vector<Edge> r_edge;
	//int *lMatch, *rMatch;
	int lnum, rnum, blknum, blksize;
	//int maxMatching;

	public:

	//int row, col, orth;
    int redundant_r, redundant_c;

	BipGraph(int _l, int _r, int _blk): lnum(_l), rnum(_r*_blk), 
								blknum(_blk), blksize( (_l-1) / _blk + 1), 
								//maxMatching(-1),
                                edge(), r_edge(),// row(0), col(0), orth(0),
                                redundant_r(0), redundant_c(0)
						{
                            lhead = new int[_l];
                            rhead = new int[_r * blknum];
							//lMatch = new int[_l];
							//visited = new bool[_r]();
							//rMatch = new int[_r];
							memset(lhead, 0xff, lnum*sizeof(int));
							memset(rhead, 0xff, rnum*sizeof(int));
							//memset(lMatch, 0xff, lnum*sizeof(int));
							//memset(rMatch, 0xff, rnum*sizeof(int));
						}

	~BipGraph() 		{
							delete[] lhead;
							delete[] rhead;
							//delete[] lMatch;
							//delete[] rMatch;
							//delete[] visited;
						}

	void addEdge(int from, int to) {
										int blk = from / blksize;
										to += blk * (rnum/blknum);
									
                                        Edge newEdge(to, lhead[from]);
                                        edge.push_back(newEdge);
                                        lhead[from] = edge.size()-1;

                                        Edge r_newEdge(from, rhead[to]);
                                        r_edge.push_back(r_newEdge);
                                        rhead[to] = r_edge.size()-1;
                                    }

	void resetMatching()
						{
							//maxMatching = -1; 
							//memset(lMatch, 0xff, lnum*sizeof(int));
							//memset(rMatch, 0xff, rnum*sizeof(int));
							//memset(visited, 0, rnum*sizeof(bool));
							//row = col = orth = 0;
                            redundant_r = redundant_c = 0;
						}

    /*void resetGraph()
                        {
                            edge.clear();
                            resetMatching();
                        }*/

	//int getMaxMatching();
	//bool tryMatching(int x);
	//void printMatching();
	//void greedy_stat();
    bool repair_most(); 
    bool tryFix();

    private:
    void remove_first(bool *lrep, bool *rrep);
    //void greedy_first(bool *lrep, bool *rrep, int limit);
    void degree_calc(bool *lrep, bool *rrep, int *ld, int *rd);
    //void remove_orth(bool *lrep, bool *rrep);
    void maxdeg_calc(int *ld, int *rd, int &lmax, int &rmax,
            int &lmv, int &rmv);
    bool check_repaired(bool *lrep, bool *rrep, int *ld, int *rd);
    bool tryFixLeft(int lVertix, bool *lrep, bool *rrep,
            int *ld, int *rd);
    //bool tryFixRight(int lVertix, bool *lrep, bool *rrep,
    //        int *ld, int *rd);
};

#endif
