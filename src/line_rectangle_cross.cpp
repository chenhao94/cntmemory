#include"line_rectangle_cross.h"

const double eps = 1e-8;
struct point
{
    double x,y;
    point(double _x=0.0,double _y=0.0):x(_x),y(_y){}
    friend istream & operator >> (istream & in,point & p)
    {
        in>>p.x>>p.y;
        return in;
    }
};
double xmult(point p1,point p2,point p0)
{
	return (p1.x-p0.x)*(p2.y-p0.y)-(p2.x-p0.x)*(p1.y-p0.y);
}
int opposite_side(point p1,point p2,point l1,point l2){
	return xmult(l1,p1,l2)*xmult(l1,p2,l2)<-eps;

}
int intersect_ex(point u1,point u2,point v1,point v2)
{
	return opposite_side(u1,u2,v1,v2)&&opposite_side(v1,v2,u1,u2);
}
int dot_online_in(point p,point l1,point l2)
{
	return zero(xmult(p,l1,l2))&&(l1.x-p.x)*(l2.x-p.x)<eps&&(l1.y-p.y)*(l2.y-p.y)<eps;
}
//point rectangle[4];
//point a,b;
int isSectionTwo(int sx,int sy, int ex, int y, int lbx, int lby, int tpx, int tpy)
{
    point rectangle[4];
    point a,b;

    a.x=sx,a.y=sy;
    rectangle[0].x=lbx,rectangle[0].y=lby;
    rectangle[2].x=tpx,rectangle[2].y=tpy;
     rectangle[1]=point(rectangle[2].x,rectangle[0].y);
    rectangle[3]=point(rectangle[0].x,rectangle[2].y);
    int cnt=0;
    for(int i=0;i<4;i++)
    {
        //if( dot_online_ex(p,rectangle[i],rectangle[(i+1)%4]) ) cnt++;
        if( intersect_ex(a,b,rectangle[i],rectangle[(i+1)%4]) ) cnt++;
        if( dot_online_in(rectangle[i],a,b ) ) cnt++;
    }
	cout<< "sx = "<< sx<< endl
		<< "sy = "<<sy <<endl
		<< "ex = "<<ex <<endl
		<< "y = "<<y <<endl
		<< "lbx = "<<lbx <<endl
		<< "lby = "<<lby<< endl
		<< "tpx = "<<tpx <<endl
		<< "tpy = "<<tpy <<endl;

    if(cnt==2)
	{
		//1 �ཻ
		cout<< "success"<<endl;
        return 1;
	}
    else
	{
		cout << "failed"<<endl;
         return 0;
	}

}