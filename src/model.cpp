#include <iostream>
#include <fstream>
#include <cmath>
#include <boost/math/distributions.hpp>
#include <boost/math/distributions/binomial.hpp>
#include "parameter.h"
#include "utils.h"

static long double err_prob[60];
static long double prob;
static long double band_size = parameter::cnt_length/parameter::sram_height;

long double q_pow(long double base, int pwr)
{
    long double ans = 1, tmp = base;
    while (pwr > 0)
    {
        if (pwr & 1)
            ans *= tmp;
        tmp *= tmp;
        pwr = pwr >> 1;
    }
    return ans;
}

long double calc_prob()
{
    using namespace boost::math;
    long double W = parameter::trans_width;
    long double miu_s = parameter::pitch;
    long double sig_s = parameter::pitch_deviation;
    long double Ps = 1 - 1/((long double)parameter::metallic_ratio); 
    long double mean = W/miu_s;
    long double deviation = W * sqr(sig_s) / cub(miu_s);
    long double prob = 0;
    for (int n = 1; n <= 30 ; ++n)
    {
        prob += (1 - q_pow(Ps, n)) * pdf(normal(mean, deviation), n);
    }
    prob = 1 - cub(1 - prob);
    return prob;
}

int count_faulty_cols(double yield)
{
    using namespace boost::math;
    double band_num = parameter::bl_length / band_size;
    if (yield < eps)
        return 0;
    int t_cols = parameter::wl_length, e_cols = t_cols;
    
    for (int i = 0; i <= t_cols; ++i)
    {
        long double c = cdf(binomial(t_cols, prob), i);
        if ( pow(c, band_num) > yield - eps)
        {
            e_cols = i;
            break;
        }
    }
    return e_cols;
}

int num_of_segs(int len, int block_len)
{
    return (len + block_len / 2) / block_len + 1;
}

int count_redundant_cols(int block, int f_cols)
{
    long double ans = 0;
    int block_len = (parameter::wl_length - 1) / block + 1;
    for (int i = 1; i <= 50; ++i)
    {
        ans += f_cols * err_prob[i] * num_of_segs(i, block_len);
    }
    return ceil(ans / block);
}

void readModelInputs(char *filename)
{
    ifstream input(filename);
    for (int i = 1; i <= 50; ++i)
        input >> err_prob[i];
    input.close();
}

void prediction()
{
    int predict[5][8];
    int block;
    double yield;
    parameter::readParameters("paras.config");
    readModelInputs("model_input.txt");
    prob = calc_prob();
    for (int i = 1; i < 5; ++i)
        for (int j = 0; j < 8; ++j)
        {
            block = 2*i;
            yield = 0.6 + 0.05*(double)j;
            int f_cols = count_faulty_cols(yield);
            predict[i][j] = count_redundant_cols(block, f_cols);
        }
    ofstream output("prediction.log");
    for (int i = 1; i < 5; ++i)
    {
        block = 2*i;
        output << "block num = " << block << std::endl;
        for (int j = 0; j < 8; ++j)
            output << predict[i][j] << std::setw(4);
        output << std::endl;
    }
    output.close();
}

void check_mc()
{
    using namespace std;
    ifstream input("mc_result.log");
    ofstream output("check_mc.log");
    char buffer[256];
    long double rate[14];
    for (int block = 1; block < 9; ++block)
    {
        input.getline(buffer, 256);
        if (block %2 == 0)
            output << "block num = " << block << std::endl;
        for (int c = 0; c < 14 ; ++c)
        {
            input.getline(buffer, 256);
            int x;
            sscanf(buffer, "%d", &x);
            rate[c] = ((double)x)/10000.;
        }
        if (block %2 == 0)
        {
            for (int j = 0; j < 8; ++j)
            {
                long double yield = 0.6 + 0.05*(double)j;
                for (int k = 0; k < 14; ++k)
                    if (rate[k] > yield - eps)
                    {
                        output << k << setw(4);
                        break;
                    }
            }
            output << endl;
        }
    }
    input.close();
    output.close();
}

int model_main()
{
    //prediction();
    check_mc();
    return 0;
}
