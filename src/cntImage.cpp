#include "cntImage.h"
#ifdef IMAGE

CNTImage::CNTImage(int _r, int _c, int _w)
{
	row = _r * (1+_w) +1;
	column = _c * (1+_w) + 1;
	width = _w;
	image = (Pixel*)malloc( row * column * sizeof(Pixel));
	for (int i = 0; i < row; ++i)
		for (int j = 0; j < column; ++j)
			setPixel(i , j, new Pixel(255,255,255));
	for (int i = 0; i <= _r; ++i)
		for (int j = 0; j < column; ++j)
			setPixel(i * (1+_w), j, new Pixel(0,0,0));
	for (int i = 0; i < row; ++i)
		for (int j = 0; j <= _c; ++j)
			setPixel(i, j*(1+_w), new Pixel(0,0,0));
}

inline void CNTImage::setPixel(int x, int y, Pixel* pixel)
{
	*(image + x * column + y) = *pixel;
}

inline Pixel *CNTImage::getPixel(int x, int y)
{
	return image + x * column + y;
}

void CNTImage::printImage(char *filename)
{
	FILE *fo = fopen(filename, "wb");
	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	int x, y;
	png_byte **row_pointers = NULL;
	int status = -1;
    int pixel_size = 3;
    int depth = 8;
	
    if (!fo)
        return;

    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL)
        goto png_create_write_struct_failed;
    
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) 
        goto png_create_info_struct_failed;
    
    if (setjmp (png_jmpbuf (png_ptr)))
        goto png_failure;
    
    png_set_IHDR (png_ptr,
                  info_ptr,
                  column,
                  row,
                  depth,
                  PNG_COLOR_TYPE_RGB,
                  PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_DEFAULT,
                  PNG_FILTER_TYPE_DEFAULT);
	
    row_pointers = (png_byte ** )png_malloc (png_ptr, row * sizeof (png_byte *));
    for (y = 0; y < row; ++y) {
        png_byte *_r = 
            (png_byte*)png_malloc (png_ptr, sizeof (uint8_t) * column * pixel_size);
        row_pointers[y] = _r;
        for (x = 0; x < column; ++x) {
			Pixel *pixel = getPixel(y, x);
            *_r++ = pixel->r;
            *_r++ = pixel->g;
            *_r++ = pixel->b;
        }
    }

    png_init_io (png_ptr, fo);
    png_set_rows (png_ptr, info_ptr, row_pointers);
    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    /* The routine has successfully written the file, so we set
       "status" to a value which indicates success. */

    status = 0;
    
    for (y = 0; y < row; y++) 
	{
        png_free (png_ptr, row_pointers[y]);
    }
    png_free (png_ptr, row_pointers);
    
png_failure:
png_create_info_struct_failed:
    png_destroy_write_struct (&png_ptr, &info_ptr);
png_create_write_struct_failed:
    fclose (fo);
}

void CNTImage::setCell(int _r, int _c, Pixel* pixel)
{
	for (int i = _r*(1+width) + 1; i < (_r+1)*(1+width); ++i)
		for (int j = _c*(1+width) + 1; j< (_c + 1)*(1+width); ++j)
			setPixel(i,j,pixel);
}

void CNTImage::setLine(int _r, Pixel* pixel)
{
	int i = (_r+1)*(1+width);
	for (int j = 0; j < column; ++j)
	{
		setPixel(i ,j, pixel);
		setPixel(i+1 ,j, pixel);
		setPixel(i-1 ,j, pixel);
	}
}
#endif
