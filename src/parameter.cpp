#include"parameter.h"
int parameter::wl_length;// = 64;
int parameter::bl_length;// = 64;
double parameter::sram_height = 200;
double parameter::sram_width = 300;
double parameter::cnt_length = 40000;
double parameter::cnt_length_deviation = 3000;
double parameter::pitch = 4;
double parameter::pitch_deviation = 2.4;
double parameter::sram_number = parameter::wl_length * parameter::bl_length;
//transistor layout parameters
double parameter::trans_height = 32;
double parameter::trans_width = 48;
double parameter::tran_height_pitch = 18;
double parameter::tran_width_pitch = 22;
int parameter::test_step = 4;
int parameter::initialization_test_step = 16;
double parameter::word_width = 8;
long parameter::metallic_ratio;// = 1000;
long parameter::slope_ratio;// = 100; 
int parameter::level_num;// = 4;
double parameter::redundant_ratio;// = 0.09;
int parameter::max_redundant_r;
int parameter::max_redundant_c;
double  PI=3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170680;
int parameter::stuck_open_switch=0;
void dump_parameter()
{
	cout << "wl_length = "<< parameter::wl_length << endl
		<< "bl_length = "<< parameter::bl_length <<endl 
		<< "sram_height = "<< parameter::sram_height << endl
		<< "sram_width = "<< parameter::sram_width << endl
		<< "cnt_length = "<< parameter::cnt_length << endl
		<< "pitch = "<< parameter::pitch << endl;
	cout << "trans_width = " << parameter::trans_width << endl;
	cout << "trans_height = " << parameter::trans_height << endl;

	ofstream output;
	output.open("Sample_normal.txt",ios::out);
	output << "************************************************************" << endl;
	output << "wl_length = "<< parameter::wl_length << endl
	<< "bl_length = "<< parameter::bl_length <<endl 
	<< "sram_height = "<< parameter::sram_height << endl
	<< "sram_width = "<< parameter::sram_width << endl
	<< "cnt_length = "<< parameter::cnt_length << endl
	<< "pitch = " << parameter::pitch << endl
	<< "trans width = " << parameter::trans_width << endl
	<< "trans height = " << parameter::trans_height << endl
	<< "metal_ratio = 0.001%" << endl;
	output << "************************************************************" << endl;
	output << "tot_sram tot_sram_defects tot_sram_defects_ratio findout_sram_defects acc_findout wrong_findout acc_findout_ratio wrong_findout_ratio"
		<< " tot_test_cost conventional_test_cost Jump/con_test_cost_ratio" << endl;
	output.close();

	output.open("Sample_Jump.txt",ios::out);
	output << "************************************************************" << endl;
	output << "wl_length = "<< parameter::wl_length << endl
	<< "bl_length = "<< parameter::bl_length <<endl 
	<< "sram_height = "<< parameter::sram_height << endl
	<< "sram_width = "<< parameter::sram_width << endl
	<< "cnt_length = "<< parameter::cnt_length << endl
	<< "pitch = " << parameter::pitch << endl
	<< "trans width = " << parameter::trans_width << endl
	<< "trans height = " << parameter::trans_height << endl
	<< "metal_ratio = 0.001%" << endl;
	output << "************************************************************" << endl;
	output << "tot_sram tot_sram_defects tot_sram_defects_ratio findout_sram_defects acc_findout wrong_findout acc_findout_ratio wrong_findout_ratio"
		<< " tot_test_cost conventional_test_cost Jump/con_test_cost_ratio" << endl;
	output.close();


}
void clear_sample_dump(vector<int> test_step_v,vector<int> jump_threshold_v,vector<long> metallic_v)
{
	vector<string> path_v;
	string algorithm;
	path_v.push_back("Word_Jump_Recursion");
	path_v.push_back("Word_Jump_Static");
	path_v.push_back("Word_Jump_Redundancy");
	//cout << "path_v.size = " << path_v.size()<<endl;
	for(vector<string>::size_type iter=0;iter<path_v.size();iter++)
	{
		
		algorithm = path_v[iter];
		string tmp_path="./"+algorithm;
		if(test_step_v.empty()!=true)
		{
			string type="test_step";
			string path=tmp_path+"/test_step/";
			
			for(vector<int>::size_type it=0;it<test_step_v.size();it++)
			{

				char version[50];
				sprintf(version, "%d", test_step_v[it]);
				string name=type +"_"+ version;
				cout <<"name = " << name<<endl;
				ofstream output;
				output.open((path+"Sample_"+name+".txt").c_str(),ios::out);
//				cout << "path = " <<path <<endl
//					<< " name = " << name<<endl;
				output.close();
			}
		}
		/*if(jump_threshold_v.empty()!=true)
		{
			string type="jump_threshold";
			string path=tmp_path+"/jump_threshold/";
			for(vector<int>::size_type it=0;it<test_step_v.size();it++)
			{
				char version[50];
				itoa(test_step_v[it],version,10);
				string name=type +"_"+ version;
				ofstream output;
				output.open(path+"Sample_"+name+".txt",ios::out);
				output.close();
			}
		}*/
		if(metallic_v.empty()!=true)
		{
			string type="metallic";
			string path=tmp_path+"/metallic/";
			for(vector<int>::size_type it=0;it<metallic_v.size();it++)
			{
				string version;
				switch(metallic_v[it])
				{
					/*case 20000: version="1";break;
					case 10000: version="2";break;
					case 6666:version="3";break;
					case 5000:version="4";break;
					case 4000:version="5";break;
					case 3333:version="6";break;*/

					case 10000: version="7";break;
					case 5000: version="8";break;
					case 2500: version="9";break;
					case 1666: version="10";break;
					case 1250: version="11";break;
					case 1000: version="12";break;

//					case 1000000:version="1000000";break;
					default:version = " false";
						cout<<"Metallic string transform has proble";;break;
				}
				string name=type +"_"+ version;
//				cout << "metallic = "<< metallic_v[it];
//				cout << "name = "<<name<<endl;
				ofstream output;
				output.open((path+"Sample_"+name+".txt").c_str(),ios::out);
				output.close();

				ofstream output1;
				path = tmp_path+"/repair/";
				output1.open((path+"Sample_"+name+".txt").c_str(),ios::out);
				output1.close();
			}
		}
	}
}

void parameter::readParameters(char *filename)
{
    ifstream input;
    input.open(filename);
    input >> bl_length >> wl_length;
    input >> level_num;
    input >> redundant_ratio;
    input >> metallic_ratio >> slope_ratio;
    input.close();
}
