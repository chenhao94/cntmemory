#include "bipartite.h"

void BipGraph::degree_calc(bool *lrep, bool *rrep, int *ld, int *rd)
{
    memset(ld, 0, sizeof(int)*256);
    memset(rd, 0, sizeof(int)*2048);

    int j;
    for (int i = 0; i < lnum; ++i)
        if (!lrep[i])
            for (int e = lhead[i]; e != -1; e = edge[e].nextEdge)
                if (!rrep[j = edge[e].dest])
                {
                    ++ld[i];
                    ++rd[j];
                }
}

void BipGraph::maxdeg_calc(int *ld, int *rd, int &lmax, int &rmax, int &lmv, int &rmv)
{
    lmax = rmax = 0;
    for (int i = 0; i < lnum; ++i)
        if (ld[i] > lmax)
        {
            lmax = ld[i];
            lmv = i;
        }
    for (int i = 0; i < rnum; ++i)
        if (rd[i] > rmax)
        {
            rmax = rd[i];
            rmv = i;
        }
}

/*bool BipGraph::repair_most()
{
	bool lrep[256]={0}, rrep[256]={0};
	int ld[256]={0}, rd[256]={0};
	int lmax, rmax, lmv, rmv, max;

    remove_first(lrep, rrep);
    if (redundant_r < 0 || redundant_c < 0)
        return false;

    while (redundant_r > 0 && 
            redundant_c > 0 && max > 1)
    {
        degree_calc(lrep, rrep, ld, rd);
        maxdeg_calc(ld, rd, lmax, rmax, lmv, rmv);
		if (lmax > rmax && lmax > 1)
		{
			--redundant_r;
			lrep[lmv] = true;
		}
		else if (rmax >= lmax && rmax > 1 )
		{
			--redundant_c;
			rrep[rmv] = true;
		}
		max = lmax > rmax? lmax : rmax;
    }
#ifdef DEBUG
    bool ret =  check_repaired(lrep, rrep);
    using namespace std;
    cout << "row: " ;
    for (int i = 0; i< lnum; ++i)
        if (lrep[i])
            cout << i << " ";
    cout << endl;
    cout << "col: " ;
    for (int i = 0; i< lnum; ++i)
        if (rrep[i])
            cout << i << " ";
    cout << endl;
    cout << "rows remain: " << redundant_r << endl;
    cout << "cols remain: " << redundant_c << endl;
    return ret;
#else
	return check_repaired(lrep, rrep);
#endif
}*/

bool BipGraph::check_repaired(bool *lrep, bool *rrep, int *ld, int *rd)
{
	int last = 0, cnt, now;
	int col_group_size = rnum/blknum;
	for (int i = 0; i < blknum; ++i)
	{
		/*if ( i%2 == 0) // share in 2
		{
			cnt = 0;
			now = 0;
		}*/

        cnt = 0;
		for (int j = i * col_group_size; j < (i+1)*col_group_size; ++j)
			if (rrep[j] || rd[j] > 0)
				++cnt;
		//now += redundant_c; // share in 2

        // ----- share neighbor ---
		now = redundant_c; 
		if (last < 0)
		{
			now += last;
			if (now < 0)
				return false;
		}
		else
		{
			cnt -= last;
			if (cnt < 0)
				cnt = 0;
		}

		last = now - cnt;
        // ----- end of share neighbor --

		/*if (i%2 == 1)
		{
			if (cnt > now)
				return false;
		}*/ //share in 2
	}
	//if (cnt <= now) // share in 2
    if (last >= 0) // share neighbor
	{
#ifdef DEBUG
		using namespace std;
		cout << "repaired rows: ";
		for (int i = 0; i <= lnum; ++i)
			if (lrep[i])
				cout << "{" << i/blksize <<
					", " << i%blksize << "} ";
		cout << endl;
#endif
		return true;
	}
    return false; 
}

void BipGraph::remove_first(bool *lrep, bool *rrep)
{
    int ld[256]={0}, rd[2048]={0};
    int row = redundant_r, col = redundant_c;
    degree_calc(lrep, rrep, ld, rd);
    for (int i = 0; i<rnum; ++i)
        if (rd[i] > row)
        {
            rrep[i] = true;
        }
}

bool BipGraph::tryFix()
{
    bool lrep[256]={0}, rrep[2048]={0};
    int ld[256]={0}, rd[2048]={0};
#ifdef DEBUG
    std::cout << "[debug] redundant_r " << redundant_r << std::endl;
    std::cout << "[debug] redundant_c " << redundant_c << std::endl;
#endif
    remove_first(lrep, rrep);
#ifdef DEBUG
    std::cout << "fix cols: ";
	int col_group_size = rnum/blknum;
	std::cout << "cgsize: " << col_group_size << std::endl;
    for (int i = 0 ; i < rnum; ++i)
        if (rrep[i])
            std::cout << " {" << i/col_group_size
				<< ", " << i%col_group_size << "}";
    std::cout << std::endl;
#endif
    degree_calc(lrep, rrep, ld, rd);
	bool ret;
#ifdef DEBUG
	int lcnt = 0;
	for (int i = 0; i<lnum ; ++i)
		if (!lrep[i] && ld[i] > 0)
			++lcnt;
	int rcnt = 0;
	for (int i = 0; i<rnum ; ++i)
		if (!rrep[i] && rd[i] > 0)
			++rcnt;
	std::cout << "[debug] lcnt number: " << lcnt << std::endl;
	std::cout << "[debug] rcnt number: " << rcnt << std::endl;
#endif
	ret = tryFixLeft(0, lrep, rrep, ld, rd);
#ifdef DEBUG
	if ( ret )
		std::cout << "[debug] fix success" << std::endl;
	else
		std::cout << "[debug] fix failed" << std::endl;
#endif
	return ret;
}

bool BipGraph::tryFixLeft(int lVertix, 
        bool *lrep, bool *rrep, int *ld, int *rd)
{
    while (lVertix < lnum && 
            (lrep[lVertix] || !ld[lVertix]))
        ++lVertix;
#ifdef DEBUG
//    std::cout << "[debug] tryFixLeft: " << lVertix << std::endl;
#endif
    if (lVertix >= lnum || redundant_r == 0)
    {
		return check_repaired(lrep, rrep, ld, rd);
    }

    if (redundant_r > 0)
    {
        lrep[lVertix] = true;
        --redundant_r;
        int j;
#ifdef DEBUG
        //std::cout << "[debug] tryFixLeft: " << lVertix << std::endl; 
#endif
        for (int e = lhead[lVertix]; e != -1; e = edge[e].nextEdge)
            if (!rrep[j = edge[e].dest])
                --rd[j];
        if (tryFixLeft(lVertix + 1, lrep, rrep, ld, rd))
            return true;
        for (int e = lhead[lVertix]; e != -1; e = edge[e].nextEdge)
            if (!rrep[j = edge[e].dest])
                ++rd[j];
        lrep[lVertix] = false;
        ++redundant_r;
    }
    return tryFixLeft(lVertix + 1, lrep, rrep, ld, rd);
}
