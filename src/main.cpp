#include "model.h"

int mem_main();

int main()
{
#ifdef MODEL
    return model_main();
#else
    return mem_main();
#endif
}
