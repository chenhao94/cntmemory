// intersect.cpp : 定义控制台应用程序的入口点。
//
#include"intersect.h"




struct Point
{
	double x;
	double y;
};

double max(double x, double y)
{
	return ((x>y)? x:y);
}

int corner_case(struct Point a, struct Point b, struct Point p, struct Point q)
{
	int result = 0;
	struct Point c;   //矩形的另外两个端点
	struct Point d;
	c.x = b.x;
	c.y = a.y;
	d.x = a.x;
	d.y = b.y;
	double a1, b1, c1; //线段所在直线的参数
	if (p.x == q.x)  //计算线段所在直线参数
	{
		b1 = 0;
		a1 = 1;
		c1 = -(p.x);
	}
	else
	{
		a1 = (q.y - p.y) / (q.x - p.x);
		b1 = -1;
		c1 = q.y - a1*q.x;
	}
	if ((a1*a.x + b1*a.y + c1) == 0 && (a.x <= max(p.x, q.x)) && (a.y <= max(p.y, q.y)))
		result++;
	if ((a1*b.x + b1*b.y + c1) == 0 && (b.x <= max(p.x, q.x)) && (b.y <= max(p.y, q.y)))
		result++;
	if ((a1*c.x + b1*c.y + c1) == 0 && (c.x <= max(p.x, q.x)) && (c.y <= max(p.y, q.y)))
		result++;
	if ((a1*d.x + b1*d.y + c1) == 0 && (d.x <= max(p.x, q.x)) && (d.y <= max(p.y, q.y)))
		result++;
	return result;
}

bool is_line_intersect(struct Point a, struct Point b, struct Point p, struct Point q)
{
	double a1, b1, c1; //第一条线段所在直线的参数
	double a2, b2, c2; //第二条线段所在直线的参数
	if (a.x == b.x)  //计算第一条直线的参数
	{
		b1 = 0;
		a1 = 1;
		c1 = -(a.x);
	}
	else
	{
		a1 = (b.y - a.y) / (b.x - a.x);
		b1 = -1;
		c1 = b.y - a1*b.x;
	}
	if (p.x == q.x)  //计算第二条直线的参数
	{
		b2 = 0;
		a2 = 1;
		c2 = -(p.x);
	}
	else
	{
		a2 = (q.y - p.y) / (q.x - p.x);
		b2 = -1;
		c2 = q.y - a2*q.x;
	}
	if (((a1*p.x + b1*p.y + c1)*(a1*q.x + b1*q.y + c1)<=0) && ((a2*a.x + b2*a.y + c2)*(a2*b.x + b2*b.y + c2)<=0))//两条线段相交的充要条件
		return true;
	else
		return false;
}

bool is_intersect(struct Point a, struct Point b, struct Point p, struct Point q)  //判断线段和矩形是否相交（此处相交是指线段的穿过矩阵，线段的一个端点在矩阵的边上不算穿过）
{
	int counter = 0;
	struct Point c;   //矩形的另外两个端点
	struct Point d;
	c.x = b.x;
	c.y = a.y;
	d.x = a.x;
	d.y = b.y;
	if (is_line_intersect(a, d, p, q))  //判断矩形的四条边与线段的相交情况
		counter++;
	if (is_line_intersect(b, c, p, q))
		counter++;
	if (is_line_intersect(a, c, p, q))
		counter++;
	if (is_line_intersect(b, d, p, q))
		counter++;
	int result = corner_case(a, b, p, q);
	if (counter < 2)
		return false;
	else if (counter == 2 && result > 0)
		return false;
	else if (counter == 3 && result == 2)
		return false;
	else 
		return true;
}

bool is_intersect_t(double line_st_x,double line_st_y,double line_end_x,double line_end_y,double left_bottom_x,double left_bottom_y,double right_top_x,double right_top_y)
{
	struct Point a;   //矩形的两个点
	struct Point b;
	struct Point p;   //线段的两个端点
	struct Point q;
	a.x=line_st_x;
	a.y =line_st_y;

	b.x=line_end_x;
	b.y =line_end_y ;

	p.x=left_bottom_x;
	p.y =left_bottom_y ;

	q.x=right_top_x;
	q.y =right_top_y ;


	bool result = is_intersect(a,b,p,q);

	return result;
}

