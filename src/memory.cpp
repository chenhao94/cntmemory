/*
class CNT:member variable, catalyze band number, catalyze band position, cnt length, cnt position,
*/
#include "memory.h"
#include "utils.h"
#include "omp.h"
//#include"Word_oriented test.h"


double gaussrand(double E, double sigma)
{
	double rand_v = 0;
	double rand_u = 0;
	double u = 0;
	double v = 0;
	double s = 0;
	int i = 0;
	int j = 0;
	int NUM_MAX = 1;
	double rand_gauss = 0;
	double gauss_delay = 0;
	//	srand(time(0));
	for(i = 0; i < 1; i ++)
	{
		do{
			rand_v = (double)rand()/RAND_MAX;
			rand_u = (double)rand()/RAND_MAX;
			u = rand_u * 2 - 1;
			v = rand_v * 2 - 1;
			s = u * u + v * v;
		}while(s >= 1 || s == 0);
		rand_gauss = u * sqrt(-2 * log(s) / s);
		//		cout<<(rand_gauss * sigma + E)<<endl;
	}
	/*if(rand()%2==0)
		rand_gauss = rand_gauss * sigma + E;
	else
		rand_gauss = E - rand_gauss * sigma;*/
	rand_gauss = E - rand_gauss * sigma;
	return rand_gauss;
}
vector<cnt>::size_type transistor::find_first_cnt(vector<cnt>::size_type start,vector<cnt>::size_type end,int &num_stripe,vector< vector<cnt> > &_cnt,double &left_bottom_x,double &right_top_x)
{
	//std::cout << "debug 2 "<<  start << " " << end << std::endl;
	if (end == (vector<cnt>::size_type)-1)
		return 0;
	if(start == end)
		return start;
	else
	{
		vector<cnt>::size_type mid = (start + end)/2;
		//std::cout << num_stripe << " " << mid << std::endl;
		double pos = _cnt[num_stripe][mid].get_position();
		if(pos < left_bottom_x)
			return find_first_cnt(mid+1,end,num_stripe,_cnt,left_bottom_x,right_top_x);
		else if(pos == left_bottom_x)
			return mid;
		else if(pos > left_bottom_x)
		{
			if(mid == 0)
				return mid;
			else if(_cnt[num_stripe][mid-1].get_position()< left_bottom_x)
				return mid;
			else
				return  find_first_cnt(start,mid,num_stripe,_cnt,left_bottom_x,right_top_x);
		}
	}
}
void sram::trans_defect_to_sram_fault()
{
	const int trans_per_sram = TRANS_PER_SRAM;
	bool sram_trans_functional[trans_per_sram];
	for(int i=0;i<trans_per_sram;++i)
	{
		if(m_trans[i].get_functional()==true)
			sram_trans_functional[i] = true;
		else
			sram_trans_functional[i] = false;
	}
	//m-CNT affect two CNFETs in one column
	if( (sram_trans_functional[0] == false && sram_trans_functional[1] == false) ||
		(sram_trans_functional[2] == false && sram_trans_functional[3] == false) )
	{
		m_functional=false;
		defect=STUCK_AT_1;
	}
	else if(sram_trans_functional[4] == false && sram_trans_functional[5] == false)
	{
		m_functional=false;
		defect=STUCK_AT_0;
	}
	//clope m-CNT affect two CNFETs in different columns
	else if( (sram_trans_functional[2] == false && sram_trans_functional[5] == false) ||
		(sram_trans_functional[0] == false && sram_trans_functional[3] == false) )
	{
		m_functional=false;
		defect=STUCK_AT_1;
	}
	else if( (sram_trans_functional[3] == false && sram_trans_functional[4] == false) ||
		(sram_trans_functional[1] == false && sram_trans_functional[2] == false) )
	{
		m_functional=false;
		defect=STUCK_AT_0;
	}
	else if( sram_trans_functional[0] == false ||
		sram_trans_functional[2] == false ||
		sram_trans_functional[5] == false)
	{
		m_functional=false;
		defect=STUCK_AT_1;
	}
	else if( sram_trans_functional[4] == false ||
		sram_trans_functional[1] == false ||
		sram_trans_functional[3] == false)
	{
		m_functional=false;
		defect=STUCK_AT_0;
	}
}
bool memory::get_sram_functional(int row,int column)
{
	return _sram[row][column].is_functional();
}
int memory::get_sram_fault(int row,int column)
{
	return _sram[row][column].get_defect();
}
//***************************************************************************
//override dump function
void test_result::dump(memory &mem,string name,bool rand_fault_map[][256])
{
	ofstream output;
	output.open(("Test result_"+name+".txt").c_str(),ios::out);
	output << "Start output memory initialization parameter......" << endl;
	time_t t;
	time ( &t );
	output << "The current date/time is: "<<ctime(&t);

	output << "************************************************************" << endl;
	output << "wl_length = "<< parameter::wl_length << endl
	<< "bl_length = "<< parameter::bl_length <<endl 
	<< "sram_height = "<< parameter::sram_height << endl
	<< "sram_width = "<< parameter::sram_width << endl
	<< "cnt_length = "<< parameter::cnt_length << endl
	<< "pitch = " << parameter::pitch << endl
	<< "trans width = " << parameter::trans_width << endl
	<< "trans height = " << parameter::trans_height << endl;
	output << "************************************************************" << endl;
	output << "Start output findout sram defects......" << endl;
	long acc_findout = 0;
	long wrong_findout = 0;
	for(int row=0;row<parameter::bl_length;row++)
	{
		output <<  "row = " << std::left << setw(7) << row;
		for(int col=0;col<parameter::wl_length;++col)
		{
			if(sram_tag[row][col]==false)
			{
				{
					output << setw(3)<<"F";
					if(mem.get_sram_functional(row,col) == false)
					{
						if(rand_fault_map[row][col]==false)
							++acc_findout;
					}
					else
						++wrong_findout;
				}
			}
			else	
				output << setw(3)<<"-";
			if(col%8 == 7)
			{
				//three blanks
				output << "   ";
			}
		}
		output << endl;

		int tmp = parameter::cnt_length / parameter::sram_height;
		if (row%tmp == tmp-1)
			output << endl;
	}
	output << "************************************************************" << endl;
	output << "tot_sram = " << parameter::sram_number << endl;
	output << "tot_sram_defects = " << mem.get_tot_sram_defects() << endl;
	output << "tot_sram_defects_ratio = " <<  ((double)mem.get_tot_sram_defects())/(parameter::bl_length * parameter::wl_length) << endl;
	if(mem.get_tot_sram_defects()!=0)
	{
		output << "fault_coverage = " << ((double)acc_findout / mem.get_tot_sram_defects()) << endl;
	}
	else
	{
		output << "fault_coverage =  1" << endl;
	}
	output << "findout_sram_defects = " << get_findout_sram_defects() << endl;
	output << "acc_findout = " << acc_findout << endl;
	output << "wrong_findout = " << wrong_findout << endl;
	if(get_findout_sram_defects()!=0)
	{
		output << "acc_findout_ratio = " << ((double)acc_findout / get_findout_sram_defects()) << endl;
		output << "wrong_findout_ratio = " << ((double)wrong_findout / get_findout_sram_defects()) << endl;
	}
	else
	{
		output << "acc_findout_ratio = 1" << endl;
		output << "wrong_findout_ratio = 0" << endl;
	}
	output << "tot_test_cost = " << get_test_cost() << endl;
	output << "conventional_test_cost = " << 4*parameter::sram_number << endl;
	output << "Jump/con_test_cost_ratio = " << get_test_cost() / (4*parameter::sram_number) <<endl;
	output.close();
}
void test_result::sample_dump(memory &mem,string name,bool rand_fault_map[][256])
{
	tag_fault_coveragge = true;
	ofstream output;
	output.open(("Sample_"+name+".txt").c_str(),ios::app);
	long acc_findout = 0;
	long wrong_findout = 0;
//	output << "Start output wrong test bits......" << endl;
	for(int row=0;row<parameter::bl_length;row++)
	{
		for(int col=0;col<parameter::wl_length;++col)
		{
			if(sram_tag[row][col]==false)
			{
				{
					if(mem.get_sram_functional(row,col) == false)
					{
						if(rand_fault_map[row][col]==false)
							++acc_findout;
					}
					else
					{
						++wrong_findout;
						//cout <<"row = " << row << " col = " << col << endl;
					}
				}
			}
		}
	}
	output << parameter::sram_number << " "
		<< mem.get_tot_sram_defects() << " "
		<< ((double)mem.get_tot_sram_defects())/(parameter::bl_length * parameter::wl_length) << " ";
		if(mem.get_tot_sram_defects()!=0)
		{
			output << ((double)acc_findout / mem.get_tot_sram_defects()) << " ";
		}
		else
		{
			output << "1 ";
		}
		output<< get_findout_sram_defects() << " "
		<< acc_findout << " "
		<< wrong_findout << " ";
		if(get_findout_sram_defects()!=0)
		{
			output << ((double)acc_findout / get_findout_sram_defects()) << " ";
			output << ((double)wrong_findout / get_findout_sram_defects()) << " ";
		}
		else
		{
			output << "1 ";
			output << "0 ";
		}
		output << get_test_cost() << " "
		<< 4*parameter::sram_number << " "
		 << get_test_cost() / (4*parameter::sram_number) << endl;
	output.close();
	if(acc_findout != mem.get_tot_sram_defects())
	{
		tag_fault_coveragge = false;
		//cout << "This iteration failed!!!!!!!!!!!!!!!!!!!!!!!!!";
	}
}
//***************************************************************************
//override dump function
void test_result::dump(memory &mem,string name,string path,bool rand_fault_map[][256])
{
	ofstream output;
	output.open((path+"Test result_"+name+".txt").c_str(),ios::out);
	output << "Start output memory initialization parameter......" << endl;
	time_t t;
	time ( &t );
	output << "The current date/time is: "<<ctime(&t);

	output << "************************************************************" << endl;
	output << "wl_length = "<< parameter::wl_length << endl
	<< "bl_length = "<< parameter::bl_length <<endl 
	<< "sram_height = "<< parameter::sram_height << endl
	<< "sram_width = "<< parameter::sram_width << endl
	<< "cnt_length = "<< parameter::cnt_length << endl
	<< "pitch = " << parameter::pitch << endl
	<< "trans width = " << parameter::trans_width << endl
	<< "trans height = " << parameter::trans_height << endl;
	output << "************************************************************" << endl;
	output << "Start output findout sram defects......" << endl;
	long acc_findout = 0;
	long wrong_findout = 0;
	for(int row=0;row<parameter::bl_length;row++)
	{
		output <<  "row = " << std::left << setw(7) << row;
		for(int col=0;col<parameter::wl_length;++col)
		{
			if(sram_tag[row][col]==false)
			{
				{
					output << setw(3)<<"F";
					if(mem.get_sram_functional(row,col) == false)
					{
						if(rand_fault_map[row][col]==false)
							++acc_findout;
					}
					else
						++wrong_findout;
				}
			}
			else	
				output << setw(3)<<"-";
			if(col%8 == 7)
			{
				//three blanks
				output << "   ";
			}
		}
		output << endl;

		int tmp = parameter::cnt_length / parameter::sram_height;
		if (row%tmp == tmp-1)
			output << endl;
	}
	output << "************************************************************" << endl;
	output << "tot_sram = " << parameter::sram_number << endl;
	output << "tot_sram_defects = " << mem.get_tot_sram_defects() << endl;
	output << "tot_sram_defects_ratio = " <<  ((double)mem.get_tot_sram_defects())/(parameter::bl_length * parameter::wl_length) << endl;
	if(mem.get_tot_sram_defects()!=0)
	{
		output << "fault_coverage = " << ((double)acc_findout / mem.get_tot_sram_defects()) << endl;
	}
	else
	{
		output << "fault_coverage =  1" << endl;
	}
	output << "findout_sram_defects = " << get_findout_sram_defects() << endl;
	output << "acc_findout = " << acc_findout << endl;
	output << "wrong_findout = " << wrong_findout << endl;
	if(get_findout_sram_defects()!=0)
	{
		output << "acc_findout_ratio = " << ((double)acc_findout / get_findout_sram_defects()) << endl;
		output << "wrong_findout_ratio = " << ((double)wrong_findout / get_findout_sram_defects()) << endl;
	}
	else
	{
		output << "acc_findout_ratio = 1" << endl;
		output << "wrong_findout_ratio = 0" << endl;
	}
	output << "tot_test_cost = " << get_test_cost() << endl;
	output << "conventional_test_cost = " << 4*parameter::sram_number << endl;
	output << "Jump/con_test_cost_ratio = " << get_test_cost() / (4*parameter::sram_number) <<endl;
	output.close();
}
void test_result::sample_dump(memory &mem,string name,string path,bool rand_fault_map[][256])
{
	tag_fault_coveragge = true;
	ofstream output;
	output.open((path+"Sample_"+name+".txt").c_str(),ios::app);
	long acc_findout = 0;
	long wrong_findout = 0;
//	output << "Start output wrong test bits......" << endl;
	for(int row=0;row<parameter::bl_length;row++)
	{
		for(int col=0;col<parameter::wl_length;++col)
		{
			if(sram_tag[row][col]==false)
			{
				{
					if(mem.get_sram_functional(row,col) == false)
					{
						if(rand_fault_map[row][col]==false)
							++acc_findout;
					}
					else
					{
						++wrong_findout;
						//cout <<"row = " << row << " col = " << col << endl;
					}
				}
			}
		}
	}
	output << parameter::sram_number << " "
		<< mem.get_tot_sram_defects() << " "
		<< ((double)mem.get_tot_sram_defects())/(parameter::bl_length * parameter::wl_length) << " ";
		if(mem.get_tot_sram_defects()!=0)
		{
			output << ((double)acc_findout / mem.get_tot_sram_defects()) << " ";
		}
		else
		{
			output << "1 ";
		}
		output<< get_findout_sram_defects() << " "
		<< acc_findout << " "
		<< wrong_findout << " ";
		if(get_findout_sram_defects()!=0)
		{
			output << ((double)acc_findout /get_findout_sram_defects()) << " ";
			output << ((double)wrong_findout / get_findout_sram_defects()) << " ";
		}
		else
		{
			output << "1 ";
			output << "0 ";
		}
		output << get_test_cost() << " "
		<< 4*parameter::sram_number << " "
		 << get_test_cost() / (4*parameter::sram_number) << endl;
	output.close();
	if(acc_findout != mem.get_tot_sram_defects())
	{
		tag_fault_coveragge = false;
		//cout << "This iteration failed!!!!!!!!!!!!!!!!!!!!!!!!!";
	}
}
//***************************************************************************
void perform_Word_Deep_jump(memory &mem,vector<long> &metallic_v,vector<int> &test_step_v,vector<int> &jump_threshold_v,vector<double> &length_v,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Recursion";
	string tmp_path="./"+algorithm;

	/*if(metallic_v.empty()!=true)
	{
		string type="metallic";
		string path=tmp_path+"/metallic/";

		parameter::test_step=4;
		parameter::initialization_test_step=1000;

		for(vector<int>::size_type it=0;it<test_step_v.size();it++)
		{
			parameter::metallic_ratio = test_step_v[it];

			test_result result;
			char version[50];
			itoa(test_step_v[it],version,10);
			string name=type +"_"+ version;
			word_test_metal_recursive_Jump(mem,result);
			result.dump(mem,name,path);
			result.sample_dump(mem,name,path);

			parameter::metallic_ratio = 1000;
		}
	}*/
	if(test_step_v.empty()!=true)
	{
		string type="test_step";
		string path=tmp_path+"/test_step/";

		parameter::test_step=4;
		parameter::metallic_ratio=1000;

		for(vector<int>::size_type it=0;it<test_step_v.size();it++)
		{
			parameter::initialization_test_step = test_step_v[it];

			test_result result;
			char version[50];
			sprintf(version, "%d", test_step_v[it]);
			string name=type +"_"+ version;
			word_test_metal_recursive_Jump(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			parameter::initialization_test_step = 16;
		}
	}
//	cout << "jump_threshold =" << jump_threshold_v.empty();
	/*if(jump_threshold_v.empty()!=true)
	{
		string type="jump_threshold";
		string path=tmp_path+"/jump_threshold/";

		parameter::initialization_test_step=32;
		parameter::metallic_ratio=1000;

		for(vector<int>::size_type it=0;it<test_step_v.size();it++)
		{

			parameter::test_step = test_step_v[it];
			test_result result;
			char version[50];
			itoa(test_step_v[it],version,10);
			string name=type +"_"+ version;
			word_test_metal_recursive_Jump(mem,result);
			result.dump(mem,name,path);
			result.sample_dump(mem,name,path);

			parameter::test_step = 4;
		}
	}*/
}
void perform_Word_Static_jump(memory &mem,vector<long> &metallic_v,vector<int> &test_step_v,vector<int> &jump_threshold_v,vector<double> &length_v,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Static";
	string tmp_path="./"+algorithm;

	if(test_step_v.empty()!=true)
	{
		string type="test_step";
		string path=tmp_path+"/test_step/";

		parameter::test_step=4;
		parameter::metallic_ratio=1000;

		for(vector<int>::size_type it=0;it<test_step_v.size();it++)
		{ 
			parameter::initialization_test_step = test_step_v[it];
			parameter::test_step=test_step_v[it];
			//cout<<"test_step = " << test_step_v[it]<<endl;
			//cout <<"initialization test step = "<< parameter::initialization_test_step<<endl;
			test_result result;
			char version[50];
			sprintf(version, "%d", test_step_v[it]);
			string name=type +"_"+ version;
			word_test_staict_step(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			parameter::initialization_test_step = 16;
			parameter::test_step=4;
		}
	}
}
void perform_Word_Redundancy_jump(memory &mem,vector<long> &metallic_v,vector<int> &test_step_v,vector<int> &jump_threshold_v,vector<double> &length_v,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Redundancy";
	string tmp_path="./"+algorithm;
	if(test_step_v.empty()!=true)
	{
		string type="test_step";
		string path=tmp_path+"/test_step/";

		parameter::test_step=4;
		parameter::metallic_ratio=1000;

		for(vector<int>::size_type it=0;it<test_step_v.size();it++)
		{
			parameter::initialization_test_step = test_step_v[it];

			test_result result;
			char version[50];
			sprintf(version, "%d", test_step_v[it]);
			string name=type +"_"+ version;
			word_test_metal_Jump(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			parameter::initialization_test_step = 16;
		}
	}
}
//***************************************************************************
//overider perform function to implement metallic variation
void output_false_columns(memory &mem,bool rand_fault_map[][256],string path,string name)
{
	int rows_per_stripe = parameter::cnt_length / parameter::sram_height;
	int tot_false_columns=0;
	int detect_false_columns=0;
	ofstream output;
	output.open((path+"Sample_"+name+".txt").c_str(),ios::app);
	bool tag_tot;
	bool tag_detect;
	for(int col=0;col<parameter::wl_length;col++)
	{
		tag_tot = false;
		tag_detect = false;
		for(int row=0;row<rows_per_stripe;row++)
		{
			if(mem.get_sram_functional(row,col)==false)
			{
				tag_tot = true;
				if(rand_fault_map[row][col] == false)
					tag_detect = true;
			}
		}
		if(tag_tot == true)
			++tot_false_columns;
		if(tag_detect == true)
			++detect_false_columns;
	}
	output << tot_false_columns << " "
		<< detect_false_columns <<endl;
	output.close();

}
void perform_Word_Deep_jump(memory &mem,vector<long> &metallic_v,vector<int>::size_type it,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Recursion";
	string tmp_path="./"+algorithm;

	if(metallic_v.empty()!=true)
	{
		string type="metallic";
		string path=tmp_path+"/metallic/";
			test_result result;
			string version;
			switch(metallic_v[it])
			{
				/*case 20000: version="1";break;
					case 10000: version="2";break;
					case 6666:version="3";break;
					case 5000:version="4";break;
					case 4000:version="5";break;
					case 3333:version="6";break;*/
				case 10000: version="7";break;
					case 5000: version="8";break;
					case 2500: version="9";break;
					case 1666: version="10";break;
					case 1250: version="11";break;
					case 1000: version="12";break;


						default:version = " false";
						cout<<"Metallic string transform has proble";;break;
			}
			string name=type +"_"+ version;
			word_test_metal_recursive_Jump(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			path=tmp_path+"/repair/";
			output_false_columns(mem,rand_fault_map,path,name);
	}
}
void perform_Word_Static_jump(memory &mem,vector<long> &metallic_v,vector<int>::size_type it,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Static";
	string tmp_path="./"+algorithm;

	if(metallic_v.empty()!=true)
	{
		string type="metallic";
		string path=tmp_path+"/metallic/";

			test_result result;
			string version;
			switch(metallic_v[it])
			{
				/*case 20000: version="1";break;
					case 10000: version="2";break;
					case 6666:version="3";break;
					case 5000:version="4";break;
					case 4000:version="5";break;
					case 3333:version="6";break;*/

				case 10000: version="7";break;
					case 5000: version="8";break;
					case 2500: version="9";break;
					case 1666: version="10";break;
					case 1250: version="11";break;
					case 1000: version="12";break;
						default:version = " false";
						cout<<"Metallic string transform has proble";;break;
			}
			string name=type +"_"+ version;
			word_test_staict_step(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			path=tmp_path+"/repair/";
			output_false_columns(mem,rand_fault_map,path,name);
	}
}
void perform_Word_Redundancy_jump(memory &mem,vector<long> &metallic_v,vector<int>::size_type it,bool rand_fault_map[][256])
{
	string algorithm = "Word_Jump_Redundancy";
	string tmp_path="./"+algorithm;

	if(metallic_v.empty()!=true)
	{
		string type="metallic";
		string path=tmp_path+"/metallic/";

			test_result result;
			string version;
			switch(metallic_v[it])
			{
				/*case 20000: version="1";break;
				case 10000: version="2";break;
				case 6666:version="3";break;
				case 5000:version="4";break;
				case 4000:version="5";break;
				case 3333:version="6";break;*/

				case 10000: version="7";break;
					case 5000: version="8";break;
					case 2500: version="9";break;
					case 1666: version="10";break;
					case 1250: version="11";break;
					case 1000: version="12";break;
				default:version = " false";
					cout<<"Metallic string transform has proble";;break;
			}
			string name=type +"_"+ version;
			word_test_metal_Jump(mem,result);
			result.dump(mem,name,path,rand_fault_map);
			result.sample_dump(mem,name,path,rand_fault_map);

			path=tmp_path+"/repair/";
			output_false_columns(mem,rand_fault_map,path,name);
	}
}
//***************************************************************************
void rand_fault_injection(memory &mem,bool rand_fault_map[][256])
{
	vector<pair<int,int> > add_pair;
	getFaultPosition(add_pair,parameter::sram_number/1000,parameter::bl_length,parameter::wl_length,3,0.5);
	for(vector<pair<int,int> >::size_type iter=0;iter < add_pair.size();iter++)
	{
		int row=add_pair[iter].first;
		int col=add_pair[iter].second;
		fault_t fault;
		if(rand()%2==0)
			fault=STUCK_AT_0;
		else
			fault=STUCK_AT_1;
		if(mem.get_sram_functional(row,col)==true)
		{
			mem.rand_fault_injection(row,col,fault);
			rand_fault_map[row][col]=true;
		}
	}
}

void gen_faulty_graph(BipGraph & graph, int blk)
{
    memory mem(parameter::wl_length,parameter::bl_length,parameter::sram_height,parameter::sram_width,parameter::cnt_length,parameter::pitch);
    mem.init();
    bool rand_fault_map[256][256];
    for(int i=0;i<256;i++)
        for(int j=0;j<256;j++)
            rand_fault_map[i][j]=false;

    rand_fault_injection(mem,rand_fault_map);
    mem.print_mem(&graph, rand_fault_map, blk);
#ifdef DEBUG
	ofstream output("faulty_map", ios::out);

    for(int i=0;i<256;i++)
	{
        for(int j=0;j<256;j++)
            output << (rand_fault_map[i][j]?1:0) << " ";
		output << endl;
	}
	output.close();
#endif
}

void load_faulty_graph(BipGraph & graph, char *filename)
{
    ifstream input(filename, ios::in);
    for(int i=0;i<256;i++)
        for(int j=0;j<256;j++)
        {
            int t;
            input >> t;
            if (t > 0)
                graph.addEdge(i,j);
        }

}

//***************************************************************************
int mem_main()
{ 
	time_t t;
	time ( &t );
	cout<<"The beginning date/time is: "<<ctime(&t) << endl;

    parameter::readParameters("paras.config");

	srand(time(0));
	dump_parameter();
	bool clear_tag=false;
	int sample_number = 0;//500;

	vector<int> test_step_v,jump_threshold_v;
	vector<long> metallic_v;
	vector<double> length_v;
	fetchData(metallic_v,test_step_v,jump_threshold_v,length_v);
	if(clear_tag==false)
	{
		clear_tag=true;
		clear_sample_dump(test_step_v,jump_threshold_v,metallic_v);
	}

    double sum = 0, sqr_sum = 0;
	double srow = 0, scol = 0, sorth = 0;
	double sr_sq = 0, sc_sq = 0, so_sq = 0;
	double red_rate = parameter::redundant_ratio;
    int reps = 10000, lev_num = parameter::level_num;
    int rr_per_level = ceil(parameter::bl_length * red_rate);
    int rc_per_level = ceil(parameter::wl_length * red_rate);
    int succ[9][50][50]={0};
    parameter::max_redundant_r = rr_per_level;
    parameter::max_redundant_c = rc_per_level;

    
    #ifndef DEBUG
    #pragma omp parallel for collapse(4)
    for (int i = 0; i< reps; ++i)
		for (int blk = 2; blk < 9; blk+=2)
			for (int c = 0; c <= parameter::max_redundant_c; ++c)
				for (int r = 0; r < 1; ++r) 
    #else
    for (int i = 0, c = 4, r = 0, blk = 7; i<1 ; ++i)
    #endif
	{
		bool rep_succ;
        BipGraph graph(parameter::bl_length, parameter::wl_length, blk);
#ifdef DEBUG
        //load_faulty_graph(graph, "faulty_map");
        gen_faulty_graph(graph, blk);
#else
        gen_faulty_graph(graph, blk);
#endif
        graph.redundant_r = r;
        graph.redundant_c = c;
		rep_succ = graph.tryFix();
#ifdef DEBUG
        //cout << graph.redundant_r << " " << graph.redundant_c << endl;
        //cout << rep_succ << endl;
        if (rep_succ)
        {
            BipGraph graph1(parameter::bl_length, parameter::wl_length, blk+1);
            load_faulty_graph(graph1, "faulty_map");
            graph1.redundant_r = r;
            graph1.redundant_c = c;
            if (!graph1.tryFix())
                break;
        }
#else
        if (rep_succ)
        {
            #pragma omp atomic
            ++succ[blk][c][r];
        }
#endif
	}

#ifndef DEBUG
	ofstream output("column_block", ios::out);
	for (int i = 2; i < 9 ;i += 2)
	{
		output << "----------devide into " << i <<  " blocks------------" << endl;
		for (int c = 0; c <=parameter::max_redundant_c; ++c )
		{
			for (int r = 0; r < 1; ++r)
				output << setw(10) << succ[i][c][r];
			output << endl;
		}
	}
	output.close();
#endif

	time ( &t );
	cout<<"The ending date/time is: "<<ctime(&t) << endl;
/*
	cout << "row = " << parameter::bl_length << endl;
	cout << "col = " << parameter::wl_length << endl;
    cout << "lev = " << lev_num << endl;
	cout << "redundant row per level= " << rr_per_level << endl;
	cout << "redundant col per level= " << rc_per_level << endl;
	cout << "yield rate = " << sum/reps << endl;
    
	/*double avg = sum/reps;
    double variance = sqr_sum/reps - avg*avg;
    int max_repair = min(parameter::wl_length, parameter::bl_length);
    double rate = avg/max_repair;
    double stdvar_rate  = sqrt(variance) / max_repair;
    cout << "max repair: " << max_repair << endl;
    cout << "average min-VC: " << avg << endl;
    cout << "variance: " << variance << endl;
    cout << "rate: " << rate << endl;
    cout << "stdvar rate: " << stdvar_rate << endl;
	cout << " ------------- " << endl;

	{
		double r_avg = srow/reps;
		double r_stdvar = sqrt(sr_sq/reps - r_avg*r_avg);
		double r_rate = r_avg/parameter::bl_length;
		double r_stdvar_rate  = r_stdvar / parameter::bl_length;
		cout << "row rate: " << r_rate << " +/- " << r_stdvar_rate << endl;
	}
	
	{
		double c_avg = scol/reps;
		double c_stdvar = sqrt(sc_sq/reps - c_avg*c_avg);
		double c_rate = c_avg/parameter::wl_length;
		double c_stdvar_rate  = c_stdvar / parameter::wl_length;
		cout << "col rate: " << c_rate << " +/- " << c_stdvar_rate << endl;
	}

	{
		double o_avg = sorth/reps;
		double o_stdvar = sqrt(so_sq/reps - o_avg*o_avg);
		double o_rate = o_avg/max_repair;
		double o_stdvar_rate  = o_stdvar / max_repair;
		cout << "ort rate: " << o_rate << " +/- " << o_stdvar_rate << endl;
	}*/

	return 0;
}
