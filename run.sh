CONFIG_FILE=paras.config
level_num=4
metallic_ratio=1000
slope_ratio=100
make

for ((j=5;j<10;++j));
do
    redundant_ratio=0.0${j}
    LOG_DIR=log/repair_most_4_level_shared_upper/red_ratio_0.0${j}
    test -d ${LOG_DIR} || mkdir -p ${LOG_DIR}
    for ((i=32;i<=128;i*=2));
    do
        echo "$i $i" > ${CONFIG_FILE}
        echo "${level_num}" >> ${CONFIG_FILE}
        echo "${redundant_ratio}" >> ${CONFIG_FILE}
        echo "${metallic_ratio}" >> ${CONFIG_FILE}
        echo "${slope_ratio}" >> ${CONFIG_FILE}
        ./main > ${LOG_DIR}/report_${i}.log
    done
done

