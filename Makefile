.PHONY: all executable image debug clean before model
CC = g++
INC_DIR = src/includes
INC = -I$(INC_DIR)
FLAGS = $(INC) -fopenmp
SRC_DIR = src
OBJ_DIR = bin/objs
EXE = main

SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))

all: executable

executable: before $(OBJ_FILES)
	$(CC) $(FLAGS) $(OBJ_FILES) -o $(EXE)

image: FLAGS += -lpng -D IMAGE
image: executable

debug: CC = g++-4.4
debug: FLAGS += -g -lpng -D IMAGE -D DEBUG
debug: executable

model: FLAGS += -g -D MODEL
model: EXE = model
model: executable 

clean:
	rm -rf bin/
	rm -f $(EXE)
	rm -f $(SRC_DIR)/*~ *~

before:
	test -d $(OBJ_DIR) || mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(FLAGS) -c $< -o $@
	
